<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    // Example migration file
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('order_number');
            $table->foreignId('address_id')->nullable()->constrained()->onDelete('set null');
            $table->decimal('total_amount', 8, 2);
            $table->string('currency')->default('KZT');
            $table->string('status')->default('pending');
            $table->integer('quantity');
            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
