<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Payment;

class PaymentSeeder extends Seeder
{
    public function run()
    {
        // Создаем 10 платежей
        Payment::factory()->count(10)->create([
            'payment_method' => 'credit_card',
        ]);
    }
}
