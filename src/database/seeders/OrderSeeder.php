<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Order;
use App\Models\Address;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class OrderSeeder extends Seeder
{
    public function run()
    {
        $user = User::find(1);
        $address = Address::find(1);

        if ($user && $address) {
            // Создаем 10 заказов и сохраняем их в переменную
            $orders = Order::factory()->count(10)->create([
                'user_id' => $user->id,
                'address_id' => $address->id,
                'total_amount' => 100.00,  // Установите значение по умолчанию для total_amount
            ]);

            // Получаем первый продукт
            $product = Product::first();

            if ($product) {
                foreach ($orders as $order) {
                    // Добавляем продукт в заказ через таблицу `order_product`
                    $order->products()->attach([
                        $product->id => ['quantity' => 5],
                    ]);
                }
            } else {
                // Логируем ошибку если продукт не найден
                Log::error('Product not found for OrderSeeder.');
            }
        } else {
            // Логируем ошибку если пользователь или адрес не найдены
            Log::error('User or address not found for OrderSeeder.');
        }
    }
}
