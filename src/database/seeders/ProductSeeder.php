<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Создаем 5 категорий
        $categories = Category::factory()->count(5)->create();

        // Создаем 10 продуктов, связывая их с существующими категориями
        Product::factory()->count(10)->create([
            'category_id' => $categories->random()->id,
        ]);
    }
}