<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition(): array
    {
        $name = $this->faker->word;
        $possibleVariations = [' (детская)', ' (спальня)', ' (кухня)', 'офис'];
        $name .= $this->faker->randomElement($possibleVariations);

        return [
            'name' => $name,
            'description' => $this->faker->sentence,
            'parent_id' => null,
            'slug' => Str::slug($name . '-' . Str::random(5)),
        ];
    }
  
}
