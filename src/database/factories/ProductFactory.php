<?php


namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    public function definition(): array
    {
        $furnitureNames = [
            'Диван',
            'Стол',
            'Стул',
            'Кровать',
            'Шкаф',
            'Комод',
            'Кресло',
            'Тумба',
            'Кухня',
            'Спальня',
            'Гостиная',
            'Столовая',
            'Офисная мебель',
            'Детская мебель',
            'Садовая мебель',
            'Стелаж',
            'Вешалка',
            'Пуфик',
            'Матрас'
        ];

        return [
            'name' => $this->faker->randomElement($furnitureNames),
            'description' => $this->faker->sentence($this->faker->numberBetween(3, 5)) . ' ' . $this->faker->randomElement([
                'изготовлен из высококачественных материалов',
                'обеспечивает комфорт и функциональность',
                'впишется в любой интерьер',
                'отличается стильным дизайном',
                'идеально подходит для...',
            ]),
            'price' => $this->faker->numberBetween(1000, 10000),
            'category_id' => Category::inRandomOrder()->first()->id, // Используем случайную существующую категорию
        ];
    }
}