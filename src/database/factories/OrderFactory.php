<?php

// database/factories/OrderFactory.php

namespace Database\Factories;

use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    protected $model = Order::class;

    public function definition()
    {
        return [
            'user_id' => User::factory(),  // Генерация пользователя для каждого заказа
            'quantity' => $this->faker->numberBetween(1, 10),  // Установка случайного количества
            'address_id' => \App\Models\Address::factory(),  // Генерация адреса для каждого заказа
            'order_number' => $this->faker->unique()->word,
            'total_amount' => $this->faker->randomFloat(2, 10, 1000),
        ];
    }
}
