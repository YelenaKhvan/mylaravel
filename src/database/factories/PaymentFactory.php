<?php

namespace Database\Factories;

use App\Models\Payment;
use App\Models\Order;
use App\Models\Cart;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    protected $model = Payment::class;

    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'transaction_id' => $this->faker->uuid,
            'amount' => $this->faker->randomFloat(2, 1, 1000),
            'cart_id' => Cart::factory(),
            'user_id' => User::factory(),
        ];
    }
}
