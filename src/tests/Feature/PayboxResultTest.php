<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Order;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Category; // Добавьте это
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PayboxResultTest extends TestCase
{
    use RefreshDatabase;

    public function testSuccessfulPaymentResult()
    {
        $this->withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);
        // Создаем пользователя, корзину и заказ
        $user = User::factory()->create();
        $category = Category::factory()->create(); // Создаем категорию
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $product = Product::factory()->create(['category_id' => $category->id]); // Указываем категорию
        $cart->products()->attach($product, ['quantity' => 1]);
        $order = Order::factory()->create(['order_number' => 'ORDER123', 'user_id' => $user->id, 'total_amount' => 100]);

        $response = $this->actingAs($user)->postJson(route('paybox.result'), [
            '_token' => csrf_token(),
            'pg_status' => 'ok',
            'pg_order_id' => 'ORDER123',
            'pg_payment_id' => 'PAY123',
            'cart_id' => $cart->id,
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('orders', ['order_number' => 'ORDER123', 'status' => 'paid']);
        $this->assertDatabaseHas('payments', ['transaction_id' => 'PAY123', 'order_id' => $order->id, 'status' => 'completed']);
    }

    public function testFailedPaymentResult()
    {
        $this->withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);
        // Создаем пользователя, корзину и заказ
        $user = User::factory()->create();
        $category = Category::factory()->create(); // Создаем категорию
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $product = Product::factory()->create(['category_id' => $category->id]); // Указываем категорию
        $cart->products()->attach($product, ['quantity' => 1]);
        $order = Order::factory()->create(['order_number' => 'ORDER123', 'user_id' => $user->id, 'total_amount' => 100]);

        $response = $this->actingAs($user)->postJson(route('paybox.result'), [
            'pg_status' => 'error',
            'pg_order_id' => 'ORDER123',
            'pg_payment_id' => 'PAY123',
            'cart_id' => $cart->id,
        ]);

        $response->assertStatus(400);
        $this->assertDatabaseHas('orders', ['order_number' => 'ORDER123', 'status' => 'pending']);
    }
}
