<?php

return [
    'gateway_url' => env('PAYBOX_GATEWAY_URL', 'https://api.paybox.money'),
    'merchant_id' => env('PAYBOX_MERCHANT_ID', ''),
    'secret_key' => env('PAYBOX_SECRET_KEY', ''),
    'salt' => env('PAYBOX_SALT', ''),
    'test_mode' => env('PAYBOX_TEST_MODE', true),
    'currency' => env('PAYBOX_CURRENCY', 'KZT'),
    'pg_result_url' => env('PAYBOX_RESULT_URL', config('app.url') . '/paybox/result'),
    'pg_success_url' => env('PAYBOX_SUCCESS_URL', config('app.url') . '/paybox/success'),
    'pg_failure_url' => env('PAYBOX_FAILURE_URL', config('app.url') . '/paybox/failure'),
];
