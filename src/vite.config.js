import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    server: {
        hmr: {
            host: 'localhost'
        }
    },
    base: '/mylaravel/public/',
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
        vue(),
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
    build: {
        outDir: '../public', // Изменено на корректный путь к public директории Laravel
        assetsDir: 'js/assets', // Добавлен assetsDir для указания пути к статическим ресурсам
        rollupOptions: {
            input: {
                app: 'resources/js/app.js'
            }
        }
    }
});
