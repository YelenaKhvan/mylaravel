<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PayboxController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ShippingController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\InventoryController;

Route::prefix('admin')->group(function () {
    // Public Routes
    Route::get('/register', [AuthController::class, 'showRegisterForm'])->name('register.form');
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::get('/login', [AuthController::class, 'showLoginForm'])->name('login.form');
    Route::post('/login', [AuthController::class, 'login'])->name('login');

    // Routes that require authentication
    Route::middleware('auth')->group(function () {
        // Admin Routes with prefix '/admin'
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
        Route::get('/', [HomeController::class, 'index'])->name('home');

        // User Routes
        Route::prefix('users')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('users.index');
            Route::get('/export', [UserController::class, 'export'])->name('users.export');
            Route::get('/create', [UserController::class, 'create'])->name('users.create');
            Route::post('/', [UserController::class, 'store'])->name('users.store');
            Route::get('/{id}', [UserController::class, 'show'])->name('users.show');
            Route::get('/{id}/edit', [UserController::class, 'edit'])->name('users.edit');
            Route::patch('/{id}', [UserController::class, 'update'])->name('users.update');
            Route::delete('/{id}', [UserController::class, 'destroy'])->name('users.destroy');
        });

        // Product Routes
        Route::prefix('products')->group(function () {
            Route::get('/', [ProductController::class, 'index'])->name('products.index');
            Route::get('/create', [ProductController::class, 'create'])->name('products.create');
            Route::post('/', [ProductController::class, 'store'])->name('products.store');
            Route::get('/{id}', [ProductController::class, 'show'])->name('products.show');
            Route::get('/{id}/edit', [ProductController::class, 'edit'])->name('products.edit');
            Route::patch('/{id}', [ProductController::class, 'update'])->name('products.update');
            Route::delete('/{id}', [ProductController::class, 'destroy'])->name('products.destroy');
        });

        // Category Routes
        Route::prefix('categories')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('categories.index');
            Route::get('/create', [CategoryController::class, 'create'])->name('categories.create');
            Route::post('/', [CategoryController::class, 'store'])->name('categories.store');
            Route::get('/{id}', [CategoryController::class, 'show'])->name('categories.show');
            Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
            Route::put('/{id}', [CategoryController::class, 'update'])->name('categories.update');
            Route::delete('/{id}', [CategoryController::class, 'destroy'])->name('categories.destroy');
        });

        // Cart Routes
        Route::prefix('carts')->group(function () {
            Route::get('/', [CartController::class, 'index'])->name('carts.index');
            Route::get('/create', [CartController::class, 'create'])->name('carts.create');
            Route::post('/', [CartController::class, 'store'])->name('carts.store');
            Route::get('/{id}', [CartController::class, 'show'])->name('admin.carts.show');;

            Route::get('/{id}/edit', [CartController::class, 'edit'])->name('carts.edit');
            Route::put('/{id}', [CartController::class, 'update'])->name('carts.update');
            Route::delete('/{id}', [CartController::class, 'destroy'])->name('carts.destroy');

            Route::post('/{cart}/add-product/{product}', [CartController::class, 'addProduct'])->name('carts.add-product');
            Route::delete('/{cart}/remove-product/{product}', [CartController::class, 'removeProduct'])->name('carts.remove-product');
            Route::put('/{cart}/update-product-quantity/{product}', [CartController::class, 'updateCartItemQuantity'])->name('carts.update-product-quantity');

            Route::get('/{cart}/pay', [CartController::class, 'pay'])->name('carts.pay');
        });

        // Order Routes
        Route::prefix('/orders')->group(function () {
            Route::get('/', [OrderController::class, 'index'])->name('orders.index');
            Route::get('/create', [OrderController::class, 'create'])->name('orders.create');
            Route::post('/', [
                OrderController::class, 'store'
            ])->name('orders.store');
            Route::get('/{order}', [OrderController::class, 'show'])->name('orders.show');
            Route::get('/{order}/edit', [OrderController::class, 'edit'])->name('orders.edit');
            Route::put('/{order}', [OrderController::class, 'update'])->name('orders.update');
            Route::delete('/{order}', [OrderController::class, 'destroy'])->name('orders.destroy');
            Route::delete('/{order}/cancel', [OrderController::class, 'cancel'])->name('orders.cancel');
        });



        // Address Routes
        Route::prefix('addresses')->group(function () {
            Route::get('/', [AddressController::class, 'index'])->name('addresses.index');
            Route::get('/create', [AddressController::class, 'create'])->name('addresses.create');
            Route::post('/', [AddressController::class, 'store'])->name('addresses.store');
            Route::get('/{id}', [AddressController::class, 'show'])->name('addresses.show');
            Route::get('/{id}/edit', [AddressController::class, 'edit'])->name('addresses.edit');
            Route::patch('/{id}', [AddressController::class, 'update'])->name('addresses.update');
            Route::delete('/{id}', [AddressController::class, 'destroy'])->name('addresses.destroy');
        });

        // Payment Routes
        Route::prefix('payments')->group(function () {
            Route::get('/', [PaymentController::class, 'index'])->name('payments.index');
            Route::get('/create', [PaymentController::class, 'create'])->name('payments.create');
            Route::post('/', [PaymentController::class, 'store'])->name('payments.store');
            Route::get('/{payment}/edit', [PaymentController::class, 'edit'])->name('payments.edit');
            Route::put('/{payment}', [PaymentController::class, 'update'])->name('payments.update');
            Route::delete('/{payment}', [PaymentController::class, 'destroy'])->name('payments.destroy');
        });

        // Shipping Routes
        Route::prefix('shippings')->group(function () {
            Route::get('/', [ShippingController::class, 'index'])->name('shippings.index');
            Route::get('/create', [ShippingController::class, 'create'])->name('shippings.create');
            Route::post('/', [ShippingController::class, 'store'])->name('shippings.store');
            Route::get('/{shipping}', [ShippingController::class, 'show'])->name('shippings.show');
            Route::get('/{shipping}/edit', [ShippingController::class, 'edit'])->name('shippings.edit');
            Route::put('/{shipping}', [ShippingController::class, 'update'])->name('shippings.update');
            Route::delete('/{shipping}', [ShippingController::class, 'destroy'])->name('shippings.destroy');
        });

        // Inventory Routes
        Route::prefix('inventories')->group(function () {
            Route::get('/', [InventoryController::class, 'index'])->name('inventories.index');
            Route::get('/create', [InventoryController::class, 'create'])->name('inventories.create');
            Route::post('/', [InventoryController::class, 'store'])->name('inventories.store');
            Route::get('/{inventory}', [InventoryController::class, 'show'])->name('inventories.show');
            Route::get('/{inventory}/edit', [InventoryController::class, 'edit'])->name('inventories.edit');
            Route::put('/{inventory}', [InventoryController::class, 'update'])->name('inventories.update');
            Route::delete('/{inventory}', [InventoryController::class, 'destroy'])->name('inventories.destroy');
        });
        // Paybox Routes
        Route::prefix('paybox')->group(function () {
            Route::post('/create', [PayboxController::class, 'create'])->name('paybox.create');
            Route::post('/refund', [PayboxController::class, 'refund'])->name('paybox.refund');
            Route::get('/payment-success', [PayboxController::class, 'success'])->name('paybox.success');
            Route::get('/payment-failure', [PayboxController::class, 'failure'])->name('paybox.failure');
            Route::post('/result', [PayboxController::class, 'result'])->name('paybox.result');
            Route::post('/process-payment/{cart}', [PayboxController::class, 'processPayment'])->name('paybox.processPayment');
        });

        // General payment success and failure routes
        Route::get('/payment/success', [PayboxController::class, 'success'])->name('payment.success');
        Route::get('/payment/fail', [PayboxController::class, 'fail'])->name('payment.fail');
    });
});

Route::get('/', [MainController::class, 'index'])->name('main');
