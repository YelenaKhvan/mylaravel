import { createRouter, createWebHistory } from 'vue-router';
import Get from './views/Get.vue';
import Login from './components/Auth/Login.vue';
import Register from './components/Auth/Register.vue';
import Home from './views/Home.vue';
import ListProducts from './views/Product/ListProducts.vue';
import ProductDetail from './views/Product/ProductDetail.vue';
import ListCategories from './views/Category/ListCategories.vue';
import CategoryProducts from './views/Category/CategoryProducts.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: Home,
      name: 'home',
    },
    {
      path: '/get',
      component: Get,
      name: 'get.index'
    },
    {
      path: '/user/login',
      component: Login,
      name: 'user.login'
    },
    {
      path: '/user/register',
      component: Register,
      name: 'user.register'
    },
    {
      path: '/products',
      name: 'products.index',
      component: ListProducts
    },
    {
      path: '/products/:id',
      name: 'product.detail',
      component: ProductDetail
    },
    {
      path: '/categories',
      name: 'categories.index',
      component: ListCategories
    },
    {
      path: '/categories/:id',
      name: 'category.products',
      component: CategoryProducts
    },
  ]
});

export default router;
