@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Payment</h1>

        @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif

        <form action="{{ route('payments.update', $payment->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="order_id">Order ID</label>
                <input type="text" name="order_id" id="order_id" class="form-control"
                    value="{{ old('order_id', $payment->order_id) }}" required>
            </div>
            <div class="form-group">
                <label for="user_id">User ID</label>
                <input type="text" name="user_id" id="user_id" class="form-control"
                    value="{{ old('user_id', $payment->user_id) }}" required>
            </div>
            <div class="form-group">
                <label for="amount">Amount</label>
                <input type="text" name="amount" id="amount" class="form-control"
                    value="{{ old('amount', $payment->amount) }}" required>
            </div>
            <div class="form-group">
                <label for="payment_method">Payment Method</label>
                <input type="text" name="payment_method" id="payment_method" class="form-control"
                    value="{{ old('payment_method', $payment->payment_method) }}" required>
            </div>
            <div class="form-group">
                <label for="transaction_id">Transaction ID</label>
                <input type="text" name="transaction_id" id="transaction_id" class="form-control"
                    value="{{ old('transaction_id', $payment->transaction_id) }}" required>
            </div>
            <button type="submit" class="btn btn-primary">Update Payment</button>
        </form>
    </div>
@endsection
