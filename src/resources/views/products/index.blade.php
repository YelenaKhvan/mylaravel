@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Продукты</h1>
    @if (Auth::user()->is_admin)  
    <a href="{{ route('products.create') }}" class="btn btn-primary mt-3 mb-3">Добавить продукт</a>
    @endif
    <ul class="list-group">
        @foreach ($products as $product)
        <li class="list-group-item">
            <a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a>
            <p>{{ $product->description }}</p>
            <p>{{ $product->price }} $</p>
            <p>Категория: {{ $product->category->name }}</p>
        </li>
        @endforeach
    </ul>
</div>
@endsection
