@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Продукт: {{ $product->name }}</h1>
        <p><strong>Описание:</strong> {{ $product->description }}</p>
        <p><strong>Цена:</strong> {{ $product->price }}</p>
        <p><strong>Категория:</strong> {{ $product->category->name }}</p>

        @can('update', $product)
            <a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning">Редактировать</a>
        @endcan

        @can('delete', $product)
            <form action="{{ route('products.destroy', $product->id) }}" method="POST" style="display:inline-block;">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Удалить</button>
            </form>
        @endcan

        <!-- Отладочная информация -->
        @can('view', $product)
            <p>Текущий пользователь имеет доступ к просмотру продукта.</p>
            @if ($product->inCart())
                <p>Продукт уже добавлен в корзину.</p>
                <!-- Удалить из корзины -->
                @can('removeProduct', $cart)
                    <form action="{{ route('carts.remove-product', ['cart' => $cart->id, 'product' => $product->id]) }}"
                        method="POST" style="display:inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-secondary">Удалить из корзины</button>
                    </form>
                @endcan
            @else
                <p>Продукт не добавлен в корзину.</p>
                <!-- Добавить в корзину -->
                @can('addProduct', $cart)
                    <form action="{{ route('carts.add-product', ['cart' => $cart->id, 'product' => $product->id]) }}" method="POST"
                        style="display:inline-block;">
                        @csrf
                        <input type="hidden" name="quantity" value="1">
                        <button type="submit" class="btn btn-primary">Добавить в корзину</button>
                    </form>
                @endcan
            @endif
        @else
            <p>Текущий пользователь не имеет доступа к просмотру продукта.</p>
        @endcan

        <!-- Отображение сообщений об успехе -->
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
@endsection
