@extends('layouts.app')

@section('title', 'Список категорий')

@section('content')
    <div class="container">
        <h1>Категории</h1>

        @if (Auth::user()->is_admin)
            <a href="{{ route('categories.create') }}" class="btn btn-primary mb-3">Добавить категорию</a>
        @endif

        <table class="table table-bordered mt-3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Slug</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>
                            <a href="{{ route('categories.show', $category->id) }}">{{ $category->name }}</a>
                        </td>
                        <td>{{ $category->slug }}</td>
                        <td>
                            <a href="{{ route('categories.edit', $category->id) }}"
                                class="btn btn-warning btn-sm">Редактировать</a>
                            <form action="{{ route('categories.destroy', $category->id) }}" method="POST"
                                style="display:inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">Категории не найдены.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
