@extends('layouts.app')

@section('title', 'Категория: ' . $category->name)

@section('content')
    <div class="container">
        <h1>Категория: {{ $category->name }}</h1>
        <h2>Продукты</h2>
        <div class="row">
            @forelse ($category->products as $product)
                <div class="col-md-4">
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a>
                            </h5>
                            <p class="card-text">Цена: {{ $product->price }} $</p>
                        </div>
                    </div>
                </div>
            @empty
                <p>Нет продуктов в этой категории.</p>
            @endforelse
        </div>
        <a href="{{ route('categories.index') }}" class="btn btn-primary mt-3">Назад к категориям</a>
    </div>
@endsection
