@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Shipping</h1>

        <form action="{{ route('shippings.update', $shipping->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" value="{{ $shipping->address }}"
                    required>
            </div>

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" class="form-control" value="{{ $shipping->city }}"
                    required>
            </div>

            <div class="form-group">
                <label for="postal_code">Postal Code</label>
                <input type="text" name="postal_code" id="postal_code" class="form-control"
                    value="{{ $shipping->postal_code }}" required>
            </div>

            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" class="form-control" value="{{ $shipping->country }}"
                    required>
            </div>

            <div class="form-group">
                <label for="shipping_fee">Shipping Fee</label>
                <input type="number" name="shipping_fee" id="shipping_fee" class="form-control"
                    value="{{ $shipping->shipping_fee }}" step="0.01" required>
            </div>

            <div class="form-group">
                <label for="order_total">Order Total</label>
                <input type="number" name="order_total" id="order_total" class="form-control"
                    value="{{ $shipping->order_total }}" step="0.01" required>
            </div>

            <div class="form-group">
                <label for="is_free">Free Shipping?</label>
                <input type="checkbox" name="is_free" id="is_free" value="1"
                    {{ $shipping->is_free ? 'checked' : '' }}>
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection
