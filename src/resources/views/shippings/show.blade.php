@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Shipping Details</h1>

        <p><strong>ID:</strong> {{ $shipping->id }}</p>
        <p><strong>User:</strong> {{ $shipping->user->name }}</p>
        <p><strong>Address:</strong> {{ $shipping->address->full_address }}</p>
        <p><strong>Amount:</strong> {{ $shipping->amount }}</p>

        <a href="{{ route('shippings.edit', $shipping->id) }}" class="btn btn-primary">Edit Shipping</a>
        <form action="{{ route('shippings.destroy', $shipping->id) }}" method="POST" style="display: inline-block;">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete Shipping</button>
        </form>
    </div>
@endsection
