@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Shippings</h1>

        <a href="{{ route('shippings.create') }}" class="btn btn-primary mb-3">Create Shipping</a>

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User</th>
                    <th>Address</th>
                    <th>Amount</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($shippings as $shipping)
                    <tr>
                        <td>{{ $shipping->id }}</td>
                        <td>{{ $shipping->user->name }}</td>
                        <td>{{ $shipping->address->full_address }}</td>
                        <td>{{ $shipping->amount }}</td>
                        <td>
                            <a href="{{ route('shippings.show', $shipping->id) }}" class="btn btn-sm btn-info">View</a>
                            <a href="{{ route('shippings.edit', $shipping->id) }}" class="btn btn-sm btn-primary">Edit</a>
                            <form action="{{ route('shippings.destroy', $shipping->id) }}" method="POST"
                                style="display: inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger"
                                    onclick="return confirm('Are you sure?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
