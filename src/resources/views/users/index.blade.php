@extends('layouts.app')

@section('title')
    List of users
@endsection

@section('content')
    <div class="mb-3">
        <h1>Users</h1>
        <form action="{{ route('users.index') }}" method="GET" class="mb-3">
            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label for="sort_by" class="col-form-label">Сортировать по:</label>
                </div>
                <div class="col-auto">
                    <select name="sort_by" id="sort_by" class="form-select">
                        <option value="name" {{ request()->input('sort_by') === 'name' ? 'selected' : '' }}>Имя
                        </option>
                        <option value="created_at" {{ request()->input('sort_by') === 'created_at' ? 'selected' : '' }}>
                            Дата
                        </option>
                    </select>
                </div>
                <div class="col-auto">
                    <label for="sort_order" class="col-form-label">Порядок сортировки:</label>
                </div>
                <div class="col-auto">
                    <select name="sort_order" id="sort_order" class="form-select">
                        <option value="asc" {{ request()->input('sort_order') === 'asc' ? 'selected' : '' }}>По
                            возрастанию</option>
                        <option value="desc" {{ request()->input('sort_order') === 'desc' ? 'selected' : '' }}>По
                            убыванию
                        </option>
                    </select>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Применить</button>
                </div>
                <div class="col-auto">
                    <a href="{{ route('users.export') }}" class="btn btn-primary">Download Excel</a>
                </div>
            </div>
        </form>

        @if (Auth::user()->is_admin)
            <a href="{{ route('users.create') }}" class="btn btn-success mb-3">Create User</a>
            <ul class="list-group">
                @foreach ($users as $user)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div>
                            <a href="{{ route('users.show', $user) }}" class="text-decoration-none">{{ $user->name }}</a>
                        </div>
                        <div>
                            <a href="{{ route('users.edit', $user) }}" class="btn btn-primary btn-sm">Edit</a>
                            <form action="{{ route('users.destroy', $user) }}" method="POST" style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
        <div class="mt-3">
            {{ $users->appends(request()->input())->links() }}
        </div>
    </div>
@endsection
