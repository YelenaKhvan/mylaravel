@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Address</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        <form action="{{ route('addresses.update', $address->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <input type="hidden" name="user_id" value="{{ $address->user_id }}">
            <div class="form-group">
                <label for="street">Street</label>
                <input type="text" name="street" id="street" class="form-control"
                    value="{{ old('street', $address->street) }}" required>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" class="form-control"
                    value="{{ old('city', $address->city) }}" required>
            </div>
            <div class="form-group">
                <label for="state">State</label>
                <input type="text" name="state" id="state" class="form-control"
                    value="{{ old('state', $address->state) }}" required>
            </div>
            <div class="form-group">
                <label for="postal_code">Postal Code</label>
                <input type="text" name="postal_code" id="postal_code" class="form-control"
                    value="{{ old('postal_code', $address->postal_code) }}" required>
            </div>
            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" class="form-control"
                    value="{{ old('country', $address->country) }}" required>
            </div>
            <button type="submit" class="btn btn-primary">Update Address</button>
        </form>
    </div>
@endsection
