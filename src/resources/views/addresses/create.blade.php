@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add New Address</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('addresses.store') }}" method="POST">
            @csrf
            <input type="hidden" name="user_id" value="{{ auth()->id() }}">
            <div class="form-group">
                <label for="street">Street</label>
                <input type="text" name="street" id="street" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="building_number">Building Number</label>
                <input type="text" name="building_number" id="building_number" class="form-control">
            </div>
            <div class="form-group">
                <label for="apartment_number">Apartment Number</label>
                <input type="text" name="apartment_number" id="apartment_number" class="form-control">
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="state">Region</label>
                <input type="text" name="state" id="state" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="postal_code">Postal Code</label>
                <input type="text" name="postal_code" id="postal_code" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="is_default">Default Address:</label>
                <input type="checkbox" name="is_default" id="is_default" value="1">
            </div>
            <button type="submit" class="btn btn-primary">Add Address</button>
        </form>

    @endsection
