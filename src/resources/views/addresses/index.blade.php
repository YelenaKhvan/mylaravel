@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Addresses</h1>
        <a href="{{ route('addresses.create') }}" class="btn btn-primary">Add New Address</a>

        <table class="table mt-3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Street</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Postal Code</th>
                    <th>Country</th>
                    <th>Building number</th>
                    <th>Apartment number</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($addresses as $address)
                    <tr>
                        <td>{{ $address->id }}</td>
                        <td>{{ $address->user->name }}</td>
                        <td>{{ $address->street }}</td>
                        <td>{{ $address->city }}</td>
                        <td>{{ $address->state }}</td>
                        <td>{{ $address->postal_code }}</td>
                        <td>{{ $address->country }}</td>
                        <td>{{ $address->building_number }}</td>
                        <td>{{ $address->apartment_number }}</td>
                        <td>
                            <a href="{{ route('addresses.show', $address->id) }}" class="btn btn-info">View</a>
                            <a href="{{ route('addresses.edit', $address->id) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('addresses.destroy', $address->id) }}" method="POST"
                                style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
