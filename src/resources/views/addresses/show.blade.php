@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Address Details</h1>

        <table class="table">
            <tr>
                <th>ID</th>
                <td>{{ $address->id }}</td>
            </tr>
            <tr>
                <th>Street</th>
                <td>{{ $address->street }}</td>
            </tr>
            <tr>
                <th>City</th>
                <td>{{ $address->city }}</td>
            </tr>
            <tr>
                <th>State</th>
                <td>{{ $address->state }}</td>
            </tr>
            <tr>
                <th>Postal Code</th>
                <td>{{ $address->postal_code }}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{{ $address->country }}</td>
            </tr>
            <tr>
                <th>Building number</th>
                <td>{{ $address->building_number }}</td>
            </tr>
            <tr>
                <th>Apartment</th>
                <td>{{ $address->apartment_number }}</td>
            </tr>
        </table>

        <a href="{{ route('addresses.index') }}" class="btn btn-primary">Back to Addresses</a>
    </div>
@endsection
