@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Ваша корзина</h1>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if ($cart->products->isEmpty())
            <p>Корзина пуста</p>
        @else
            <ul>
                @php
                    $totalPrice = 0;
                @endphp
                @foreach ($cart->products as $product)
                    <li>
                        <h3>{{ $product->name }}</h3>
                        <p>Описание: {{ $product->description }}</p>
                        <p>Цена: {{ $product->price }}</p>
                        <p>Количество:
                        <form
                            action="{{ route('carts.update-product-quantity', ['cart' => $cart->id, 'product' => $product->id]) }}"
                            method="POST" style="display: inline;">
                            @csrf
                            @method('PUT')
                            <input type="number" name="quantity" value="{{ $product->pivot->quantity }}" min="1"
                                max="99">
                            <button type="submit" class="btn btn-info btn-sm">Обновить</button>
                        </form>
                        </p>
                        <form action="{{ route('carts.remove-product', ['cart' => $cart->id, 'product' => $product->id]) }}"
                            method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Удалить из корзины</button>
                        </form>
                    </li>
                    @php
                        $totalPrice += $product->price * $product->pivot->quantity;
                    @endphp
                @endforeach
            </ul>
            <p>Общая сумма: {{ $totalPrice }}</p>
            @if ($defaultAddress)
                <div class="address">
                    <h3>Адрес доставки</h3>
                    <p>{{ $defaultAddress->street }}, {{ $defaultAddress->building_number }},
                        {{ $defaultAddress->apartment_number }}</p>
                    <p>{{ $defaultAddress->city }}, {{ $defaultAddress->state }}, {{ $defaultAddress->postal_code }}</p>
                    <p>{{ $defaultAddress->country }}</p>
                    <a href="{{ route('addresses.edit', ['id' => $defaultAddress->id]) }}"
                        class="btn btn-info">Редактировать адрес</a>
                </div>
            @else
                <p>Адрес не указан.</p>
            @endif



            <a href="{{ route('products.index') }}" class="btn btn-primary">Продолжить покупки</a>

            <form action="{{ route('carts.pay', ['cart' => $cart->id]) }}">
                @csrf
                <input type="hidden" name="amount" value="{{ $totalPrice }}">
                <input type="hidden" name="payment_method" value="default_method">
                <input type="hidden" name="transaction_id" value="{{ uniqid() }}">
                <input type="hidden" name="cart_id" value="{{ $cart->id }}">
                <button type="submit" class="btn btn-success">Оплатить</button>
            </form>
        @endif
    </div>
@endsection
