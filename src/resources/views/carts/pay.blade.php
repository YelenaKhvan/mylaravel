@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Подтверждение заказа</h1>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if ($cart->products->isEmpty())
            <p>Корзина пуста</p>
        @else
            <ul>
                @php
                    $totalPrice = 0;
                @endphp
                @foreach ($cart->products as $product)
                    <li>
                        <h3>{{ $product->name }}</h3>
                        <p>Описание: {{ $product->description }}</p>
                        <p>Цена: {{ $product->price }}</p>
                        <p>Количество:
                        <form
                            action="{{ route('carts.update-product-quantity', ['cart' => $cart->id, 'product' => $product->id]) }}"
                            method="POST" style="display: inline;">
                            @csrf
                            @method('PUT')
                            <input type="number" name="quantity" value="{{ $product->pivot->quantity }}" min="1"
                                max="99">
                            <button type="submit" class="btn btn-info btn-sm">Обновить</button>
                        </form>
                        </p>
                        <form action="{{ route('carts.remove-product', ['cart' => $cart->id, 'product' => $product->id]) }}"
                            method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Удалить из корзины</button>
                        </form>
                    </li>
                    @php
                        $totalPrice += $product->price * $product->pivot->quantity;
                    @endphp
                @endforeach
            </ul>
            <p>Общая сумма: {{ $totalPrice }}</p>

            @if ($cart->user->defaultAddress)
                <h2>Адрес доставки</h2>
                <p>{{ $cart->user->defaultAddress->street }}, {{ $cart->user->defaultAddress->city }},
                    {{ $cart->user->defaultAddress->state }}, {{ $cart->user->defaultAddress->postal_code }}</p>
            @else
                <p>Адрес не указан.</p>
            @endif

            <a href="{{ route('products.index') }}" class="btn btn-primary">Продолжить покупки</a>

            @auth
                <form action="{{ route('paybox.processPayment', ['cart' => $cart->id]) }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-success">Подтвердить оплату</button>
                </form>
            @else
                <p>Для выполнения оплаты необходимо <a href="{{ route('login') }}">войти</a>.</p>
            @endauth
        @endif
    </div>
@endsection
