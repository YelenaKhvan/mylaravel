@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add Inventory</h1>
        <form action="{{ route('inventories.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="product_id">Product</label>
                <select name="product_id" id="product_id" class="form-control">
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" id="quantity" class="form-control" min="0">
            </div>
            <button type="submit" class="btn btn-primary">Add Inventory</button>
        </form>
    </div>
@endsection
