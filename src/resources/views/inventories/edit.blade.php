@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Inventory</h1>
        <form action="{{ route('inventories.update', $inventory) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="product_id">Product</label>
                <select name="product_id" id="product_id" class="form-control">
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}" {{ $inventory->product_id == $product->id ? 'selected' : '' }}>
                            {{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" id="quantity" class="form-control" value="{{ $inventory->quantity }}"
                    min="0">
            </div>
            <button type="submit" class="btn btn-primary">Update Inventory</button>
        </form>
    </div>
@endsection
