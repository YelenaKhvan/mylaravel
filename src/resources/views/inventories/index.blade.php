@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Inventories</h1>
        <a href="{{ route('inventories.create') }}" class="btn btn-primary">Add Inventory</a>
        <table class="table">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($inventories as $inventory)
                    <tr>
                        <td>{{ $inventory->product->name }}</td>
                        <td>{{ $inventory->quantity }}</td>
                        <td>
                            <a href="{{ route('inventories.edit', $inventory) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('inventories.destroy', $inventory) }}" method="POST"
                                style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
