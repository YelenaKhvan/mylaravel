@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Inventory Details</h1>
        <div>
            <strong>Product:</strong> {{ $inventory->product->name }}
        </div>
        <div>
            <strong>Quantity:</strong> {{ $inventory->quantity }}
        </div>
        <a href="{{ route('inventories.index') }}" class="btn btn-primary">Back to Inventory List</a>
    </div>
@endsection
