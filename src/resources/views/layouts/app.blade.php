<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>@yield('title')</title>
    <style>
        #sidebar {
            padding-top: 60px;
            /* Отступ сверху */
            padding-left: 60px;
            /* Отступ слева */
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="container-fluid">
            <div class="row">
                <!-- Sidebar -->
                <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
                    <div class="position-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('home') }}">
                                    Главная
                                </a>
                            </li>

                            @auth
                                @if (Auth::user()->isAdmin())
                                    <li class="nav-item">
                                        <span class="nav-link">{{ Auth::user()->name }}</span>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('users.index') }}">Список пользователей</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('orders.index') }}">Список заказов</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('addresses.index') }}">Список адресов</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('payments.index') }}">Список оплат</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('shippings.index') }}">Список доставок</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('inventories.index') }}">Количество товаров</a>
                                    </li>
                                @endif
                                @if (Auth::user()->cart)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin.carts.show', Auth::user()->cart) }}">
                                            Корзина
                                        </a>
                                    </li>
                                @endif

                                <li class="nav-item">
                                    <form action="{{ route('logout') }}" method="POST" class="d-inline">
                                        @csrf
                                        <button type="submit" class="btn btn-link nav-link">
                                            {{ __('Logout') }}
                                        </button>
                                    </form>
                                </li>
                            @endauth
                            @guest
                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @endguest
                        </ul>
                    </div>
                </nav>

                <!-- Main content -->
                <main class="col-md-9 ms-sm-auto col-lg-10 px-4 mt-4">
                    @yield('content')
                </main>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
        integrity="sha384-pGm0vJERAIK2C4C7A5m6Eblc6A5gFJlO4vKuT1p9wH7eU6S5FSb62ELkDFezSgbmH" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-pzjw8f+ua7Kw1TI4eEjPwhD4y8p8d4l7CKKTdA4/zd7b5V6DE1X7oePfnR0QHnJ9" crossorigin="anonymous">
    </script>
</body>

</html>
