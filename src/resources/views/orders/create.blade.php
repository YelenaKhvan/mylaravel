@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Create Order</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('orders.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="user_id">User</label>
                <select name="user_id" id="user_id" class="form-control">
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="product_id">Product</label>
                <select name="product_id" id="product_id" class="form-control">
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" id="quantity" class="form-control" value="{{ old('quantity') }}">
            </div>
            <!-- Выбор адреса -->
            <div class="form-group">
                <label for="address_id">Address</label>
                <select name="address_id" id="address_id" class="form-control" required>
                    @foreach ($addresses as $address)
                        <option value="{{ $address->id }}">{{ $address->full_address }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Выбор метода доставки -->
            <div class="form-group">
                <label for="shipping_method">Shipping Method</label>
                <select name="shipping_method" id="shipping_method" class="form-control" required>
                    @foreach ($shippingMethods as $method)
                        <option value="{{ $method->id }}">{{ $method->name }} - {{ $method->fee }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Create Order</button>
        </form>
    </div>
@endsection
