<!DOCTYPE html>
<html>

<head>
    <title>Подтверждение заказа</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <div class="container">
        <h1>Ваш заказ успешно создан!</h1>

        <p>Заказ № {{ $order->id }} был успешно оформлен.</p>

        <h2>Детали заказа</h2>
        <ul>
            @foreach ($order->products as $product)
                <li>{{ $product->name }} - {{ $product->pivot->quantity }} x {{ $product->price }} =
                    {{ $product->pivot->quantity * $product->price }}</li>
            @endforeach
        </ul>

        <p><strong>Итоговая сумма:</strong> {{ $order->total_amount }}</p>

        <p><a href="{{ route('orders.show', ['order' => $order->id]) }}">Просмотреть заказ</a></p>
    </div>
</body>

</html>
