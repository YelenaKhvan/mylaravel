@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Order Details</h1>

        <div class="card">
            <div class="card-header">
                Order #{{ $order->id }}
            </div>
            <div class="card-body">
                @if ($order->user)
                    <h5 class="card-title">User: {{ $order->user->name }}</h5>
                @else
                    <h5 class="card-title">User: Unknown</h5>
                @endif

                @if ($order->products && $order->products->isNotEmpty())
                    <p class="card-text">Products:</p>
                    <ul>
                        @foreach ($order->products as $product)
                            <li>{{ $product->name }} (Quantity: {{ $product->pivot->quantity }})</li>
                        @endforeach
                    </ul>
                @else
                    <p class="card-text">No products found for this order.</p>
                @endif
                @if ($order->status === 'pending')
                    <form action="{{ route('orders.cancel', $order->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Отменить заказ</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
@endsection
