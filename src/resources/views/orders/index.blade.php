@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Orders</h1>
        <a href="{{ route('orders.create') }}" class="btn btn-primary mb-3">Create Order</a>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User</th>
                    <th>Products</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->user->name }}</td>
                        <td>
                            @if ($order->products->isNotEmpty())
                                <ul>
                                    @foreach ($order->products as $product)
                                        <li>{{ $product->name }} (Quantity: {{ $product->pivot->quantity }})</li>
                                    @endforeach
                                </ul>
                            @else
                                <p>No products found for this order.</p>
                            @endif
                        </td>
                        <td>
                            @can('view', $order)
                                <a href="{{ route('orders.show', $order->id) }}" class="btn btn-info">View</a>
                            @endcan
                            @can('update', $order)
                                <a href="{{ route('orders.edit', $order->id) }}" class="btn btn-warning">Edit</a>
                            @endcan
                            @can('delete', $order)
                                <form action="{{ route('orders.destroy', $order->id) }}" method="POST" style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
