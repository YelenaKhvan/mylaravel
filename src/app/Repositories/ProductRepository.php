<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class ProductRepository implements ProductRepositoryInterface
{
    public function all()
    {
        return Cache::remember('products_list', 60, function () {
            return Product::all();
        });
    }

    public function create(array $data)
    {
        $product = Product::create($data);
        // Очистить кэш после создания нового продукта
        Cache::forget('products_list');
        Log::info('Cache forgotten for products_list after creating product.');
        return $product;
    }

    public function update(Product $product, array $data)
    {
        $product->update($data);
        // Очистить кэш после обновления продукта
        Cache::forget('products_list');
        Log::info('Cache forgotten for products_list after updating product.');
        return $product;
    }

    public function delete(Product $product)
    {
        $product->delete();
        // Очистить кэш после удаления продукта
        Cache::forget('products_list');
        Log::info('Cache forgotten for products_list after deleting product.');
    }


    public function findById($id)
    {
        return Product::findOrFail($id);
    }

    public function allCategories()
    {
        // Кэшируем категории, если это требуется
        return Cache::remember('categories_list', 60, function () {
            return Category::all();
        });
    }
}
