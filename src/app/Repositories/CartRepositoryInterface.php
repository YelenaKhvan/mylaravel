<?php


namespace App\Repositories;

use App\Models\Cart;
use App\Models\User;
use App\Models\Product;

interface CartRepositoryInterface
{
    public function getAll();
    public function find($id);
    public function create(array $data);
    public function update($id, array $data);
    public function delete($id);
    public function addProductToCart(Cart $cart, $productId, $quantity);
    public function removeProductFromCart(Cart $cart, $productId);
    public function updateCartItemQuantity(Cart $cart, Product $product, $newQuantity);
}
