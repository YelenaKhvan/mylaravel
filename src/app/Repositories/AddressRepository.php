<?php

namespace App\Repositories;

use App\Models\Address;

class AddressRepository implements AddressRepositoryInterface
{
    public function getAllAddresses()
    {
        return Address::all();
    }

    public function getAddressById($id)
    {
        return Address::findOrFail($id);
    }

    public function createAddress(array $data)
    {
        return Address::create($data);
    }

    public function updateAddress(Address $address, array $data)
    {
        $address->update($data);
        return $address;
    }

    public function deleteAddress(Address $address)
    {
        return $address->delete();
    }

    public function restoreAddress($id)
    {
        $address = Address::withTrashed()->findOrFail($id);
        return $address->restore();
    }
}