<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\Product;

class CartRepository implements CartRepositoryInterface
{
    public function getAll()
    {
        return Cart::all();
    }

    public function find($id)
    {
        return Cart::find($id);
    }

    public function create(array $data)
    {
        return Cart::create($data);
    }

    public function update($id, array $data)
    {
        $cart = Cart::find($id);
        if ($cart) {
            $cart->update($data);
            return $cart;
        }

        return null;
    }

    public function delete($id)
    {
        $cart = Cart::find($id);
        if ($cart) {
            return $cart->delete();
        }

        return false;
    }

    public function addProductToCart(Cart $cart, $productId, $quantity)
    {
        $cart->products()->attach($productId, ['quantity' => $quantity]);
    }


    public function removeProductFromCart(Cart $cart, $productId)
    {
        $cart->products()->detach($productId);
    }

    public function updateCartItemQuantity(Cart $cart, Product $product, $newQuantity)
    {

        $cart->products()->updateExistingPivot($product->id, ['quantity' => $newQuantity]);
    }
}
