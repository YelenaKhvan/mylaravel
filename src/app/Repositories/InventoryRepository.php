<?php

namespace App\Repositories;

use App\Models\Inventory;

class InventoryRepository implements InventoryRepositoryInterface
{
    public function getAll()
    {
        return Inventory::all();
    }

    public function findById($id)
    {
        return Inventory::findOrFail($id);
    }

    public function create(array $data)
    {
        return Inventory::create($data);
    }

    public function update($id, array $data)
    {
        $inventory = Inventory::findOrFail($id);
        $inventory->update($data);
        return $inventory;
    }

    public function delete($id)
    {
        $inventory = Inventory::findOrFail($id);
        $inventory->delete();
    }

    public function findByProductId($productId)
    {
        return Inventory::where('product_id', $productId)->firstOrFail();
    }
}
