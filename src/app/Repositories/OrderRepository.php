<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Type\Decimal;

class OrderRepository implements OrderRepositoryInterface
{
    public function getAllOrders()
    {
        return Order::all();
    }

    public function getOrderById($id)
    {
        return Order::findOrFail($id);
    }

    public function createOrder(array $orderData)
    {
        Log::info('Creating Order with Data:', $orderData);

        try {
            $order = Order::create([
                'user_id' => (int)$orderData['user_id'],
                'order_number' => $orderData['order_number'],
                'address_id' => $orderData['address_id'] ?? null,
                'total_amount' => (float)$orderData['total_amount'],
                'currency' => $orderData['currency'] ?? 'KZT',
                'status' => $orderData['status'] ?? 'pending',
                'quantity' => (int)$orderData['quantity'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            if (isset($orderData['products']) && is_array($orderData['products'])) {
                $products = [];
                foreach ($orderData['products'] as $productId => $productData) {
                    $products[$productId] = [
                        'quantity' => (int)$productData['quantity'],
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                }
                Log::info('Syncing Products to Order:', $products);
                $order->products()->sync($products);
            } else {
                Log::warning('No products found in order data');
            }
        } catch (\Exception $e) {
            Log::error('Failed to create order', ['error' => $e->getMessage()]);
            throw $e;
        }

        Log::info('Order Created Successfully', ['order' => $order]);

        return $order;
    }



    public function updateOrder($id, array $data)
    {
        $order = Order::findOrFail($id);
        $order->update($data);
        return $order;
    }

    public function deleteOrder($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
        return $order;
    }
}
