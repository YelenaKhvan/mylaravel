<?php

namespace App\Repositories;

interface PaymentRepositoryInterface
{
    public function getAllPayments();
    public function getPaymentById($id);
    public function createPayment(array $data);
    public function updatePayment($id, array $data);
    public function deletePayment($id);
    public function getPaymentsByCartId($cartId);
}