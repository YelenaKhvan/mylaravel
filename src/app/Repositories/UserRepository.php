<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    public function getAllUsers($perPage = 10)
    {
        return User::paginate($perPage);
    }

    public function getUserById($id)
    {
        return User::findOrFail($id);
    }

    public function createUser(array $data)
    {
        return User::create($data);
    }

    public function updateUser(User $user, array $data)
    {
        return $user->update($data);
    }


    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
    }

    public function getAllUsersSorted($sortBy, $sortOrder)
    {
        // Проверяем допустимые значения для сортировки
        $validSortBy = ['name', 'email', 'created_at']; // Укажите поля, по которым можно сортировать
        $sortBy = in_array($sortBy, $validSortBy) ? $sortBy : 'name';
        $sortOrder = in_array($sortOrder, ['asc', 'desc']) ? $sortOrder : 'asc';

        return User::orderBy($sortBy, $sortOrder)->paginate(10);
    }
}
