<?php

namespace App\Repositories;

use App\Models\Category;

interface CategoryRepositoryInterface
{
    public function all();

    // Получение всех категорий с их продуктами
    public function allWithProducts();

    public function create(array $data);

    public function update(Category $product, array $data);

    public function delete(Category $product);

    public function findById($id);
}
