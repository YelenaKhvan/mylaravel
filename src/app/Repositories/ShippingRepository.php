<?php

namespace App\Repositories;

use App\Models\Shipping;
use App\Repositories\ShippingRepositoryInterface;

class ShippingRepository implements ShippingRepositoryInterface
{
    protected $model;

    public function __construct(Shipping $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return Shipping::all();
    }
    public function create(array $data): Shipping
    {
        return $this->model->create($data);
    }

    public function find($id): ?Shipping
    {
        return $this->model->find($id);
    }

    public function update($id, array $data): bool
    {
        $shipping = $this->find($id);
        return $shipping->update($data);
    }

    public function delete($id): bool
    {
        $shipping = $this->find($id);
        return $shipping->delete();
    }
}
