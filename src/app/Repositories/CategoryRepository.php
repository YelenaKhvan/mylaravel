<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\Cache;

class CategoryRepository implements CategoryRepositoryInterface
{
    // Ключ для кэша 
    protected $cacheKey = 'categories_list';
    protected $cacheWithProductsKey = 'categories_with_products';

    public function all()
    {
        // Используем Cache::remember для кэширования списка категорий
        return Cache::remember($this->cacheKey, 60, function () {
            return Category::all();
        });
    }

    public function allWithProducts()
    {
        // Используем Cache::remember для кэширования категорий с продуктами
        return Cache::remember($this->cacheWithProductsKey, 60, function () {
            return Category::with('products')->get();
        });
    }

    public function create(array $data)
    {
        $category = Category::create($data);
        // Очистить кэш после создания новой категории
        Cache::forget($this->cacheKey);
        Cache::forget($this->cacheWithProductsKey); // Очистить кэш категорий с продуктами
        return $category;
    }


    public function update(Category $category, array $data)
    {
        $category->update($data);
        // Очистить кэш после обновления категории
        Cache::forget($this->cacheKey);
        Cache::forget($this->cacheWithProductsKey); // Очистить кэш категорий с продуктами
        return $category;
    }


    public function delete(Category $category)
    {
        $category->delete();
        // Очистить кэш после удаления категории
        Cache::forget($this->cacheKey);
        Cache::forget($this->cacheWithProductsKey); // Очистить кэш категорий с продуктами
    }

    public function findById($id)
    {
        return Category::findOrFail($id);
    }
}
