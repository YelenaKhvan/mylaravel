<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\Payment;

class PaymentRepository implements PaymentRepositoryInterface
{
    public function getAllPayments()
    {
        return Payment::all();
    }

    public function getPaymentById($id)
    {
        return Payment::findOrFail($id);
    }

    public function createPayment(array $paymentData)
    {
        return Payment::create($paymentData);
    }

    public function updatePayment($id, array $data)
    {
        $payment = Payment::findOrFail($id);
        $payment->update($data);
        return $payment;
    }

    public function deletePayment($id)
    {
        $payment = Payment::findOrFail($id);
        $payment->delete();
        return $payment;
    }

    public function getPaymentsByCartId($cartId)
    {
        return Cart::findOrFail($cartId)->payments;
    }
}
