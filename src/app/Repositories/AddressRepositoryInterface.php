<?php

namespace App\Repositories;

use App\Models\Address;

interface AddressRepositoryInterface
{
    public function getAllAddresses();
    public function getAddressById($id);
    public function createAddress(array $data);
    public function updateAddress(Address $address, array $data);
    public function deleteAddress(Address $address);
    public function restoreAddress($id);
}