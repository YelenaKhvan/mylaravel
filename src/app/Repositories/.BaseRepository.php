<?php 


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository 
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($model, array $data)
    {
        $model->update($data);
        return $model;
    }

    public function delete($model)
    {
        $model->delete();
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }
}
