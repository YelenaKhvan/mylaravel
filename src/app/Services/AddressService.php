<?php

namespace App\Services;

use App\Repositories\AddressRepositoryInterface;
use App\Models\Address;

class AddressService
{
    protected $addressRepository;

    public function __construct(AddressRepositoryInterface $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    public function getAllAddresses()
    {
        return $this->addressRepository->getAllAddresses();
    }

    public function getAddressById($id)
    {
        return $this->addressRepository->getAddressById($id);
    }

    public function createAddress(array $data)
    {
        return $this->addressRepository->createAddress($data);
    }

    public function updateAddress(Address $address, array $data)
    {
        return $this->addressRepository->updateAddress($address, $data);
    }

    public function deleteAddress(Address $address)
    {
        return $this->addressRepository->deleteAddress($address);
    }

    public function restoreAddress($id)
    {
        return $this->addressRepository->restoreAddress($id);
    }
}