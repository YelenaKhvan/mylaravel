<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Product;
use App\Repositories\ProductRepositoryInterface;

class ProductService implements ProductServiceInterface
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function create(array $data)
    {
        return $this->productRepository->create($data);
    }

    public function update(Product $product, array $data)
    {
        return $this->productRepository->update($product, $data);
    }

    public function delete(Product $product)
    {
        return $this->productRepository->delete($product);
    }

    public function findById($id)
    {
        return $this->productRepository->findById($id);
    }

    public function allCategories()
    {
        return $this->productRepository->allCategories();
    }
}
