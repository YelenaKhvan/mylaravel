<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Dosarkz\Paybox\Facades\Paybox;

class PayboxService
{
    protected $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    public function createPayment($amount, $currency, $description, $orderId, $userId, $productId, $quantity, $cartId)
    {
        try {
            Log::info('Attempting to initialize payment with Paybox for order ID: ' . $orderId);
            // Инициализация платежа через Paybox
            $payment = Paybox::generate([
                'pg_order_id' => $orderId,
                'pg_amount' => $amount,
                'pg_currency' => $currency,
                'pg_description' => $description,
                'pg_merchant_id' => config('paybox.merchant_id'),
                'pg_salt' => config('paybox.salt'),
                'pg_user_ip' => request()->ip(),
                'pg_testing_mode' => config('paybox.test_mode') ? 1 : 0,
                'pg_result_url' => route('paybox.result'),
                'pg_success_url' => route('paybox.success'),
                'pg_failure_url' => route('paybox.failure'),
            ]);

            $json = json_encode($payment);
            $array = json_decode($json, true);
            //dd($array);
            Log::info('Paybox response:', ['payment' => $array]);

            if ($array['pg_status'] === 'ok') {
                return $array['pg_redirect_url'];
            } else {
                Log::error('Payment initialization failed: ' . $array['pg_error_description']);
                return false;
            }
        } catch (\Exception $e) {
            Log::error('Paybox Payment Creation Error: ' . $e->getMessage() . "\nStack trace:\n" . $e->getTraceAsString());
            return false;
        }
    }
}
