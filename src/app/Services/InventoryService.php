<?php

namespace App\Services;

use App\Models\Inventory;
use Illuminate\Support\Facades\Log;
use App\Repositories\InventoryRepositoryInterface;

class InventoryService implements InventoryServiceInterface
{
    protected $inventoryRepository;

    public function __construct(InventoryRepositoryInterface $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function getAllInventories()
    {
        return $this->inventoryRepository->getAll();
    }

    public function createInventory(array $data)
    {
        return $this->inventoryRepository->create($data);
    }

    public function updateInventory($id, array $data)
    {
        return $this->inventoryRepository->update($id, $data);
    }

    public function deleteInventory($id)
    {
        return $this->inventoryRepository->delete($id);
    }

    public function checkInventory($productId, $quantity)
    {
        $inventory = Inventory::where('product_id', $productId)->first();
        return $inventory && $inventory->quantity >= $quantity;
    }
    public function decreaseInventory(array $products)
    {
        foreach ($products as $product) {
            $inventory = Inventory::where('product_id', $product['product_id'])->first();
            if ($inventory) {
                $inventory->quantity -= $product['quantity'];
                $inventory->save();
            }
        }
    }
    public function increaseInventory(array $products)
    {
        foreach ($products as $product) {
            if (!isset($product['product_id']) || !isset($product['quantity'])) {
                Log::error('Invalid product data', $product);
                continue; // Пропустить текущий продукт, если данные некорректны
            }

            $inventory = Inventory::where('product_id', $product['product_id'])->first();

            if ($inventory) {
                $inventory->quantity += $product['quantity'];
                $inventory->save();
            } else {
                Log::warning('Inventory not found for product_id', ['product_id' => $product['product_id']]);
            }
        }
    }
}