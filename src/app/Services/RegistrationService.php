<?php

namespace App\Services;

use App\Models\User;
use App\Jobs\SendWelcomeEmail;
use App\Services\EmailService;

class RegistrationService
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function register(array $data)
    {
        // Создание пользователя
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'age' => $data['age'],
            'password' => bcrypt($data['password']),
        ]);

        // Отправка приветственного email
        SendWelcomeEmail::dispatch($user)->onQueue('mail');

        return $user;
    }
}
