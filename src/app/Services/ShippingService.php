<?php

namespace App\Services;

use App\Models\Order;
use App\Services\ShippingServiceInterface;
use App\Repositories\ShippingRepositoryInterface;

class ShippingService implements ShippingServiceInterface
{
    protected $repository;
    protected $freeShippingThreshold;

    public function __construct(ShippingRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->freeShippingThreshold = 100;
    }

    public function getAll()
    {
        return $this->repository->all();
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function create(array $data)
    {
        return $this->repository->create($data);
    }

    public function update($id, array $data)
    {
        return $this->repository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    protected function calculateShippingAmount($orderId): float
    {
        $order = Order::find($orderId);
        $orderTotal = $order->total;

        if ($orderTotal >= $this->freeShippingThreshold) {
            return 0.00;
        }

        return 10.00; // Стоимость доставки если заказ не превышает порог
    }
}
