<?php

namespace App\Services;

interface InventoryServiceInterface
{
    public function getAllInventories();
    public function createInventory(array $data);
    public function updateInventory($id, array $data);
    public function deleteInventory($id);
    public function checkInventory($productId, $quantity);
    public function decreaseInventory(array $products);
    public function increaseInventory(array $products);
}