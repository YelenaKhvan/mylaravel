<?php


namespace App\Services;

use App\Models\Category;

interface CategoryServiceInterface
{
    public function all();

    public function allWithProducts();

    public function create(array $data);

    public function update(Category $category, array $data);

    public function delete(Category $category);

    public function findById($id);
}
