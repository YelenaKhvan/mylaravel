<?php

namespace App\Services;

use App\Models\Cart;

interface PaymentServiceInterface
{
    public function getAllPayments();
    public function getPaymentById($id);
    public function createPayment(array $data);
    public function updatePayment($id, array $data);
    public function deletePayment($id);
    public function createPaymentForCart(Cart $cart, array $paymentData);
}