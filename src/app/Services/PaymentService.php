<?php

namespace App\Services;

use App\Models\Cart;
use App\Repositories\PaymentRepositoryInterface;
use App\Services\OrderServiceInterface;

class PaymentService implements PaymentServiceInterface
{
    protected $paymentRepository;
    protected $orderService;

    public function __construct(PaymentRepositoryInterface $paymentRepository, OrderServiceInterface $orderService)
    {
        $this->paymentRepository = $paymentRepository;
        $this->orderService = $orderService;
    }

    public function getAllPayments()
    {
        return $this->paymentRepository->getAllPayments();
    }

    public function getPaymentById($id)
    {
        return $this->paymentRepository->getPaymentById($id);
    }

    public function createPayment(array $data)
    {
        return $this->paymentRepository->createPayment($data);
    }

    public function updatePayment($id, array $data)
    {
        return $this->paymentRepository->updatePayment($id, $data);
    }

    public function deletePayment($id)
    {
        return $this->paymentRepository->deletePayment($id);
    }

    public function createPaymentForCart(Cart $cart, array $paymentData)
    {
        $orderData = [
            'user_id' => $cart->user_id,
            'total_amount' => $cart->totalAmount(),
        ];
        $order = $this->orderService->createOrder($orderData);

        if ($order) {
            $paymentData['order_id'] = $order->id;
            $paymentData['cart_id'] = $cart->id;
            $paymentData['amount'] = $cart->totalAmount();
            return $this->paymentRepository->createPayment($paymentData);
        }

        return null;
    }
}
