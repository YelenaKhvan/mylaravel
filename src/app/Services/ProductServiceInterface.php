<?php 

namespace App\Services;
use App\Models\Product; 

interface ProductServiceInterface
{
    public function all();

    public function create(array $data);

    public function update(Product $product, array $data);

    public function delete(Product $product);

    public function findById($id);

    public function allCategories(); 
}
