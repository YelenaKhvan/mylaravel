<?php


namespace App\Services;

use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class EmailService
{
    public function sendWelcomeEmail(User $user)
    {
        try {
            $email = new WelcomeMail($user);
            Mail::to($user->email)->send($email);
            Log::info('Welcome email sent to: ' . $user->email);
        } catch (\Exception $e) {
            Log::error('Error sending welcome email: ' . $e->getMessage());
        }
    }
}