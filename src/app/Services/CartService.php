<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\User;
use App\Models\Payment;
use App\Models\Product;
use Illuminate\Support\Facades\Log;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\PaymentRepositoryInterface;

class CartService implements CartServiceInterface
{
    protected $cartRepository;
    protected $paymentRepository;

    public function __construct(CartRepositoryInterface $cartRepository, PaymentRepositoryInterface $paymentRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public function createCart(array $data)
    {
        return $this->cartRepository->create($data);
    }

    public function updateCart($cartId, array $data)
    {
        return $this->cartRepository->update($cartId, $data);
    }

    public function deleteCart($id)
    {
        return $this->cartRepository->delete($id);
    }

    public function getCart($id)
    {
        return $this->cartRepository->find($id);
    }

    public function getAllCarts()
    {
        return $this->cartRepository->getAll();
    }

    public function addProductToCart(Cart $cart, Product $product, $quantity = 1)
    {
        Log::info('Adding product to cart', ['cart_id' => $cart->id, 'product_id' => $product->id, 'quantity' => $quantity]);

        $existingCartItem = $cart->products()->where('product_id', $product->id)->first();

        if ($existingCartItem) {
            $newQuantity = $existingCartItem->pivot->quantity + $quantity;
            $cart->products()->updateExistingPivot($product->id, ['quantity' => $newQuantity]);
        } else {
            $cart->products()->attach($product->id, ['quantity' => $quantity]);
        }
    }


    public function removeProductFromCart(Cart $cart, Product $product)
    {
        $cart->products()->detach($product->id);
    }

    public function updateCartItemQuantity(Cart $cart, Product $product, $quantity)
    {
        $cart->products()->updateExistingPivot($product->id, ['quantity' => $quantity]);
    }

    public function getOrCreateUserCart(User $user)
    {
        $cart = $user->cart;

        if (!$cart) {
            $cart = $this->cartRepository->create(['user_id' => $user->id]);
        }

        return $cart;
    }

    public function createPaymentForCart(Cart $cart, array $paymentData)
    {
        // Получаем сумму из корзины
        $paymentData['amount'] = $cart->totalAmount();

        // Создание платежа с предоставленными данными
        return $this->paymentRepository->createPayment([
            'amount' => $paymentData['amount'],
            'payment_method' => $paymentData['payment_method'],
            'transaction_id' => $paymentData['transaction_id'],
            'cart_id' => $cart->id,
            'order_id' => $paymentData['order_id'],
        ]);
    }


    public function getPaymentsForCart(Cart $cart)
    {
        return $this->paymentRepository->getPaymentsByCartId($cart->id);
    }
}
