<?php


namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;

class CategoryService implements CategoryServiceInterface
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function allWithProducts()
    {
        return $this->categoryRepository->allWithProducts();
    }

    public function create(array $data)
    {
        return $this->categoryRepository->create($data);
    }

    public function update(Category $category, array $data)
    {
        return $this->categoryRepository->update($category, $data);
    }

    public function delete(Category $category)
    {
        return $this->categoryRepository->delete($category);
    }

    public function findById($id)
    {
        return $this->categoryRepository->findById($id);
    }
}
