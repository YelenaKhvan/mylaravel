<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Log;
use App\Services\InventoryServiceInterface;
use App\Repositories\OrderRepositoryInterface;
use App\Repositories\CartRepositoryInterface;

class OrderService implements OrderServiceInterface
{
    protected $orderRepository;
    protected $inventoryService;
    protected $cartRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, InventoryServiceInterface $inventoryService, CartRepositoryInterface $cartRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->inventoryService = $inventoryService;
        $this->cartRepository = $cartRepository;
    }

    public function getAllOrders()
    {
        return $this->orderRepository->getAllOrders();
    }

    public function getOrderById($id)
    {
        return $this->orderRepository->getOrderById($id);
    }

    public function createOrder(array $orderData)
    {
        //dd($orderData);
        Log::info('OrderService::createOrder called', ['orderData' => $orderData]);

        // Генерация уникального номера заказа
        $orderData['order_number'] = 'ORD-' . strtoupper(uniqid());

        if (isset($orderData['cart_id'])) {
            $cart = $this->cartRepository->find($orderData['cart_id']);

            if ($cart) {
                $orderData['total_amount'] = $cart->totalAmount();
            }
        }

        if (!isset($orderData['total_amount'])) {
            $orderData['total_amount'] = $this->calculateTotalAmount($orderData['products'] ?? []);
        }

        $order = $this->orderRepository->createOrder($orderData);

        Log::info('Order created', ['order' => $order]);

        return $order;
    }


    public function updateOrder($id, array $data)
    {
        // Обновление заказа
        $order = $this->orderRepository->updateOrder($id, $data);

        if (isset($data['products'])) {
            $productData = collect($data['products'])->mapWithKeys(function ($product) {
                return [$product['product_id'] => ['quantity' => $product['quantity']]];
            })->toArray();

            // Логирование данных продуктов
            Log::info('Updated Product Data for Order:', $productData);

            // Обновление продуктов, связанных с заказом
            $order->products()->sync($productData);

            // Уменьшение количества продуктов на складе
            $this->inventoryService->decreaseInventory($productData);
        }

        return $order;
    }

    public function deleteOrder($id)
    {
        // Найти заказ перед удалением для удаления связанных продуктов и восстановления количества на складе
        $order = $this->orderRepository->getOrderById($id);
        if ($order) {
            // Восстановление количества продуктов на складе перед удалением заказа
            $productData = $order->products->mapWithKeys(function ($product) {
                return [$product->pivot->product_id => ['quantity' => $product->pivot->quantity]];
            })->toArray();

            $this->inventoryService->increaseInventory($productData);
        }

        // Удаление заказа
        return $this->orderRepository->deleteOrder($id);
    }

    public function calculateTotalAmount($products)
    {
        $totalAmount = 0;

        foreach ($products as $product) {
            $price = isset($product['price']) ? (float) $product['price'] : 0;
            $quantity = isset($product['quantity']) ? (int) $product['quantity'] : 0;

            $totalAmount += $price * $quantity;
        }

        Log::info('Calculated Total Amount:', ['total_amount' => $totalAmount]);

        return $totalAmount;
    }
}
