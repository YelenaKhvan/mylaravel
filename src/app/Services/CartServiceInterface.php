<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\User;
use App\Models\Product;
use App\Models\Payment;

interface CartServiceInterface
{
    public function getAllCarts();

    public function createCart(array $data);

    public function updateCart($cartId, array $data);

    public function deleteCart($cartId);

    public function addProductToCart(Cart $cart, Product $product);

    public function removeProductFromCart(Cart $cart, Product $product);

    public function updateCartItemQuantity(Cart $cart, Product $product, $quantity);

    public function getOrCreateUserCart(User $user);

    public function getPaymentsForCart(Cart $cart);

    public function createPaymentForCart(Cart $cart, array $paymentData);
}