<?php

namespace App\Services;

use App\Models\User;

interface UserServiceInterface
{
    public function getAllUsers();

    public function getUserById($id);

    public function createUser(array $data);

    public function updateUser(User $user, array $data);

    public function deleteUser(User $user);

    public function getAllUsersSorted($sortBy, $sortOrder);
}
