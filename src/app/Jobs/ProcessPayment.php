<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Dosarkz\Paybox\Facades\Paybox;
use Illuminate\Support\Facades\Log;

class ProcessPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $amount;
    protected $currency;
    protected $description;
    protected $userId;
    protected $productId;
    protected $quantity;
    protected $orderId;

    public function __construct($amount, $currency, $description, $userId, $productId, $quantity, $orderId)
    {
        $this->amount = $amount;
        $this->currency = $currency;
        $this->description = $description;
        $this->userId = $userId;
        $this->productId = $productId;
        $this->quantity = $quantity;
        $this->orderId = $orderId;
    }

    public function handle()
    {
        try {
            Log::info('Attempting to initialize payment with Paybox');

            $payment = Paybox::generate([
                'pg_order_id' => $this->orderId,
                'pg_amount' => $this->amount,
                'pg_currency' => $this->currency,
                'pg_description' => $this->description,
                'pg_merchant_id' => config('paybox.merchant_id'),
                'pg_salt' => config('paybox.salt'),
                'pg_user_ip' => request()->ip(),
                'pg_testing_mode' => config('paybox.test_mode') ? 1 : 0,
                'pg_result_url' => config('paybox.pg_result_url'),
                'pg_success_url' => config('paybox.pg_success_url'),
                'pg_failure_url' => config('paybox.pg_failure_url'),
            ]);

            $json = json_encode($payment);
            $array = json_decode($json, TRUE);

            Log::info('Paybox response:', ['payment' => $array]);

            if ($array['pg_status'] !== 'ok') {
                Log::error('Payment initialization failed: ' . $array['pg_error_description']);
            }
        } catch (\Exception $e) {
            Log::error('Paybox Payment Creation Error: ' . $e->getMessage() . "\nStack trace:\n" . $e->getTraceAsString());
        }
    }
}