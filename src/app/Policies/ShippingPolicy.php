<?php

namespace App\Policies;

use App\Models\Shipping;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShippingPolicy
{
    use HandlesAuthorization;

    /**
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shipping  $shipping
     * @return mixed
     */
    public function view(User $user, Shipping $shipping)
    {
        return $user->isAdmin() || $user->id === $shipping->user_id;
    }

    /**
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shipping  $shipping
     * @return mixed
     */
    public function update(User $user, Shipping $shipping)
    {
        return $user->isAdmin();
    }

    /**
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shipping  $shipping
     * @return mixed
     */
    public function delete(User $user, Shipping $shipping)
    {
        return $user->isAdmin();
    }
}
