<?php

namespace App\Policies;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        // Администраторы имеют полный доступ ко всем действиям с платежами
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        // Разрешить просмотр всех платежей только администраторам
        return $user->isAdmin();
    }

    public function view(User $user, Payment $payment)
    {
        // Разрешаем просмотр платежа только её владельцу или администратору
        return $user->id === $payment->user_id || $user->isAdmin();
    }

    public function create(User $user)
    {
        // Разрешаем создание платежа любому пользователю (если нужно ограничить, можно изменить логику)
        return true;
    }

    public function update(User $user, Payment $payment)
    {
        // Разрешаем обновление платежа только её владельцу или администратору
        return $user->id === $payment->user_id || $user->isAdmin();
    }

    public function delete(User $user, Payment $payment)
    {
        // Разрешаем удаление платежа только её владельцу или администратору
        return $user->id === $payment->user_id || $user->isAdmin();
    }
}