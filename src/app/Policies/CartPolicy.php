<?php

namespace App\Policies;

use App\Models\Cart;
use App\Models\User;
use App\Models\Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\Access\HandlesAuthorization;

class CartPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        // Администраторы имеют полный доступ ко всем действиям с корзинами
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function view(User $user, Cart $cart)
    {
        // Разрешаем просмотр корзины только её владельцу или администратору
        return $user->id === $cart->user_id;
    }

    public function update(User $user, Cart $cart)
    {
        // Разрешаем обновление корзины только её владельцу или администратору
        return $user->id === $cart->user_id;
    }

    public function delete(User $user, Cart $cart)
    {
        // Разрешаем удаление корзины только её владельцу или администратору
        return $user->id === $cart->user_id;
    }

    public function addProduct(User $user, Cart $cart)
    {
        // Разрешаем добавление продуктов в корзину только её владельцу или администратору
        return $user->id === $cart->user_id;
    }

    public function removeProduct(User $user, Cart $cart)
    {
        // Разрешаем удаление продуктов из корзины только её владельцу или администратору
        return $user->id === $cart->user_id;
    }

    public function create(User $user, Order $order)
    {
        Log::info('User ID: ' . $user->id . ', isAdmin: ' . ($user->isAdmin() ? 'true' : 'false'));

        // Разрешаем создание заказа только для администраторов
        return $user->isAdmin();
    }

    public function pay(User $user, Cart $cart)
    {
        return $user->id === $cart->user_id || $user->isAdmin();
    }
}
