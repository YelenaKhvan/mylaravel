<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Cart;

class PayPolicy
{
    public function before(User $user, $ability)
    {
        // Если пользователь администратор, разрешаем все действия
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function pay(User $user, Cart $cart)
    {
        // Проверяем, привязана ли корзина к пользователю
        if (!$cart->user) {
            return false;
        }

        // Проверяем, является ли текущий пользователь владельцем корзины
        return $user->id === $cart->user->id;
    }
}
