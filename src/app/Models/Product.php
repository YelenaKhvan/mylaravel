<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * schema="Product",
 * type="object",
 * title="Product",
 * description="Product model",
 * required={"name", "price", "category_id"},
 * @OA\Property(property="id", type="integer", example=1),
 * @OA\Property(property="name", type="string", example="Example Product"),
 * @OA\Property(property="description", type="string", example="This is a sample product description."),
 * @OA\Property(property="price", type="number", format="float", example=29.99),
 * @OA\Property(property="category_id", type="integer", example=2),
 * @OA\Property(property="image", type="string", example="path/to/image.jpg"),
 * )
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'price',
        'category_id',
        'image',
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_product')
            ->withPivot('quantity')
            ->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class, 'cart_product')
            ->withPivot('quantity')
            ->withTimestamps();
    }

    public function ordersAndCarts()
    {
        return $this->orders->merge($this->carts);
    }

    public function inCart()
    {
        $cart = auth()->user()->cart;
        return $cart && $cart->products->contains($this->id);
    }
}
