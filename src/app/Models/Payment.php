<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * schema="Payment",
 * type="object",
 * title="Payment",
 * description="Payment model",
 * required={"order_id", "user_id", "amount", "payment_method", "transaction_id"},
 * @OA\Property(property="id", type="integer", example=1),
 * @OA\Property(property="order_id", type="integer", example=2),
 * @OA\Property(property="user_id", type="integer", example=3),
 * @OA\Property(property="cart_id", type="integer", example=4),
 * @OA\Property(property="amount", type="number", format="float", example=199.99),
 * @OA\Property(property="payment_method", type="string", example="credit_card"),
 * @OA\Property(property="transaction_id", type="string", example="TX123456789"),
 * )
 */
class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'user_id',
        'cart_id',
        'amount',
        'payment_method',
        'transaction_id',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
