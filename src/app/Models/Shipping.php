<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * schema="Shipping",
 * type="object",
 * title="Shipping",
 * description="Shipping model",
 * required={"order_id", "address_id", "user_id", "shipping_fee", "order_total"},
 * @OA\Property(property="id", type="integer", example=1),
 * @OA\Property(property="order_id", type="integer", example=101),
 * @OA\Property(property="address_id", type="integer", example=5),
 * @OA\Property(property="user_id", type="integer", example=12),
 * @OA\Property(property="shipping_fee", type="number", format="float", example=5.99),
 * @OA\Property(property="order_total", type="number", format="float", example=105.99),
 * @OA\Property(property="is_free", type="boolean", example=false),
 * )
 */
class Shipping extends Model
{
    protected $fillable = [
        'order_id',
        'address_id',
        'user_id',
        'shipping_fee',
        'order_total',
        'is_free',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
