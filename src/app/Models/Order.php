<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * schema="Order",
 * type="object",
 * title="Order",
 * description="Order model",
 * required={"user_id", "order_number", "total_amount", "currency", "status"},
 * @OA\Property(property="id", type="integer", example=1),
 * @OA\Property(property="user_id", type="integer", example=1),
 * @OA\Property(property="order_number", type="string", example="ORD123456"),
 * @OA\Property(property="address_id", type="integer", example=2),
 * @OA\Property(property="total_amount", type="number", format="float", example=99.99),
 * @OA\Property(property="currency", type="string", example="USD"),
 * @OA\Property(property="status", type="string", example="pending"),
 * @OA\Property(property="quantity", type="integer", example=3),
 * )
 */
class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'order_number',
        'address_id',
        'total_amount',
        'currency',
        'status',
        'quantity',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product')->withPivot('quantity');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
