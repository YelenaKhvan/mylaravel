<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *     schema="Address",
 *     type="object",
 *     title="Address",
 *     description="Address model",
 *     required={"user_id", "street", "city", "state", "postal_code", "country"},
 *     @OA\Property(property="id", type="integer", example=1),
 *     @OA\Property(property="user_id", type="integer", example=1),
 *     @OA\Property(property="street", type="string", example="123 Main St"),
 *     @OA\Property(property="city", type="string", example="Springfield"),
 *     @OA\Property(property="state", type="string", example="IL"),
 *     @OA\Property(property="postal_code", type="string", example="62704"),
 *     @OA\Property(property="country", type="string", example="USA"),
 *     @OA\Property(property="building_number", type="string", example="A12"),
 *     @OA\Property(property="apartment_number", type="string", example="301"),
 *     @OA\Property(property="is_default", type="boolean", example=true),
 *     @OA\Property(property="user", ref="#/components/schemas/User")
 * )
 */
class Address extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'street',
        'city',
        'state',
        'postal_code',
        'country',
        'building_number',
        'apartment_number',
        'is_default',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
