<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * schema="Inventory",
 * type="object",
 * title="Inventory",
 * description="Inventory model",
 * required={"product_id", "quantity"},
 * @OA\Property(property="id", type="integer", example=1),
 * @OA\Property(property="product_id", type="integer", example=101),
 * @OA\Property(property="quantity", type="integer", example=50),
 * )
 */
class Inventory extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'quantity',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
