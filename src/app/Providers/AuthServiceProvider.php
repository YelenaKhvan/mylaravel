<?php

namespace App\Providers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Address;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Category;
use App\Models\Shipping;
use App\Policies\CartPolicy;
use App\Policies\AdminPolicy;
use App\Policies\OrderPolicy;
use App\Policies\AddressPolicy;
use App\Policies\PaymentPolicy;
use App\Policies\ProductPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\ShippingPolicy;
use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        User::class => AdminPolicy::class,
        Category::class => CategoryPolicy::class,
        Product::class => ProductPolicy::class,
        Cart::class => CartPolicy::class,
        Order::class => OrderPolicy::class,
        Address::class => AddressPolicy::class,
        Payment::class => PaymentPolicy::class,
        Shipping::class => ShippingPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
