<?php

namespace App\Providers;

use App\Services\CartService;
use App\Services\UserService;
use App\Services\EmailService;
use App\Services\OrderService;
use App\Services\PayboxService;
use App\Services\AddressService;
use App\Services\PaymentService;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Services\ShippingService;
use App\Services\InventoryService;
use App\Repositories\CartRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;
use App\Services\RegistrationService;
use App\Services\CartServiceInterface;
use App\Services\UserServiceInterface;
use App\Repositories\AddressRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\ProductRepository;
use App\Services\OrderServiceInterface;
use Illuminate\Support\ServiceProvider;
use App\Repositories\CategoryRepository;
use App\Repositories\ShippingRepository;
use App\Repositories\InventoryRepository;
use App\Services\AddressServiceInterface;
use App\Services\PaymentServiceInterface;
use App\Services\ProductServiceInterface;
use App\Services\CategoryServiceInterface;
use App\Services\ShippingServiceInterface;
use App\Services\InventoryServiceInterface;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;
use App\Repositories\AddressRepositoryInterface;
use App\Repositories\PaymentRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ShippingRepositoryInterface;
use App\Repositories\InventoryRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(ProductServiceInterface::class, ProductService::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(CategoryServiceInterface::class, CategoryService::class);
        $this->app->bind(CartRepositoryInterface::class, CartRepository::class);
        $this->app->bind(CartServiceInterface::class, CartService::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(OrderServiceInterface::class, OrderService::class);
        $this->app->bind(AddressRepositoryInterface::class, AddressRepository::class);
        $this->app->bind(AddressServiceInterface::class, AddressService::class);
        $this->app->bind(PaymentRepositoryInterface::class, PaymentRepository::class);
        $this->app->bind(PaymentServiceInterface::class, PaymentService::class);
        $this->app->bind(ShippingRepositoryInterface::class, ShippingRepository::class);
        $this->app->bind(ShippingServiceInterface::class, ShippingService::class);
        $this->app->bind(InventoryServiceInterface::class, InventoryService::class);
        $this->app->bind(InventoryRepositoryInterface::class, InventoryRepository::class);

        $this->app->singleton(\App\Services\EmailService::class, function ($app) {
            return new \App\Services\EmailService();
        });

        $this->app->singleton(RegistrationService::class, function ($app) {
            return new RegistrationService($app->make(EmailService::class));
        });

        $this->app->bind(PayboxService::class, function ($app) {
            return new PayboxService(
                $app->make(OrderServiceInterface::class)
            );
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}