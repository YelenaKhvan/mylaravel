<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Services\CartServiceInterface;
use App\Services\ProductServiceInterface;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $productService;
    protected $cartService;

    public function __construct(ProductServiceInterface $productService, CartServiceInterface $cartService)
    {
        $this->productService = $productService;
        $this->cartService = $cartService;
    }

    public function index()
    {
        $products = $this->productService->all();
        return view('products.index', compact('products'));
    }

    public function create()
    {
        $this->authorize('create', Product::class);
        $categories = $this->productService->allCategories();
        return view('products.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $this->authorize('create', Product::class);
        $this->productService->create($request->validated());
        return redirect()->route('products.index')->with('success', 'Продукт добавлен!');
    }

    public function show($id)
    {
        $product = $this->productService->findById($id);

        $user = auth()->user();
        $cart = $this->cartService->getOrCreateUserCart($user);

        return view('products.show', compact('product', 'cart'));
    }



    public function edit($id)
    {
        $product = $this->productService->findById($id);
        $this->authorize('update', $product);
        $categories = $this->productService->allCategories();
        return view('products.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request, $id)
    {
        $product = $this->productService->findById($id);
        $this->authorize('update', $product);
        $this->productService->update($product, $request->validated());
        return redirect()->route('products.index')->with('success', 'Продукт обновлен!');
    }

    public function destroy($id)
    {
        $product = $this->productService->findById($id);
        $this->authorize('delete', $product);
        $this->productService->delete($product);
        return redirect()->route('products.index')->with('success', 'Продукт удален!');
    }
}
