<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Services\AddressService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddressRequest;

class AddressController extends Controller
{
    protected $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    public function index()
    {
        $this->authorize('viewAny', Address::class);

        $addresses = $this->addressService->getAllAddresses();
        return view('addresses.index', compact('addresses'));
    }

    public function show($id)
    {
        $address = $this->addressService->getAddressById($id);
        $this->authorize('view', $address);

        return view('addresses.show', compact('address'));
    }

    public function create()
    {
        $this->authorize('create', Address::class);

        return view('addresses.create');
    }

    public function store(AddressRequest $request)
    {
        $this->authorize('create', Address::class);

        $data = $request->validated();
        $data['user_id'] = auth()->id();

        // Проверяем, установлено ли значение is_default и корректируем его
        $data['is_default'] = isset($data['is_default']) && $data['is_default'] ? 1 : 0;

        // Если новый адрес устанавливается как дефолтный, то делаем все другие адреса не дефолтными
        if ($data['is_default']) {
            Address::where('user_id', auth()->id())->update(['is_default' => 0]);
        }

        $this->addressService->createAddress($data);

        return redirect()->route('addresses.index');
    }


    public function edit($id)
    {
        $address = $this->addressService->getAddressById($id);
        $this->authorize('update', $address);

        return view('addresses.edit', compact('address'));
    }

    public function update(AddressRequest $request, $id)
    {
        $validatedData = $request->validated();

        // Попытка получить адрес по id
        $address = $this->addressService->getAddressById($id);

        // Проверка, был ли адрес найден
        if (!$address) {
            return redirect()->route('addresses.index')->with('error', 'Address not found.');
        }

        // Проверка авторизации
        $this->authorize('update', $address);

        // Убираем пустые значения и неизмененные поля из массива $validatedData
        foreach ($validatedData as $key => $value) {
            if ($value === '' || $value === null || $value === $address->$key) {
                unset($validatedData[$key]);
            }
        }

        // Обновление адреса
        $this->addressService->updateAddress($address, $validatedData);

        return redirect()->route('addresses.index')->with('success', 'Address updated successfully.');
    }


    public function destroy($id)
    {
        $address = $this->addressService->getAddressById($id);
        $this->authorize('delete', $address);

        $this->addressService->deleteAddress($address);
        return redirect()->route('addresses.index');
    }

    public function restore($id)
    {
        $address = Address::onlyTrashed()->findOrFail($id);
        $this->authorize('restore', $address);

        $this->addressService->restoreAddress($id);
        return redirect()->route('addresses.index');
    }
}