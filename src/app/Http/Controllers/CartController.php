<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Address;
use App\Models\Payment;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\CartRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PaymentRequest;
use App\Services\CartServiceInterface;

class CartController extends Controller
{
    protected $cartService;

    public function __construct(CartServiceInterface $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        $this->authorize('viewAny', Cart::class);
        $carts = $this->cartService->getAllCarts();
        return view('carts.index', compact('carts'));
    }

    public function create()
    {
        $this->authorize('create', Cart::class);
        return view('carts.create');
    }

    public function store(CartRequest $request)
    {
        $this->authorize('create', Cart::class);
        $this->cartService->createCart($request->validated());
        return redirect()->route('carts.index')->with('success', 'Cart created successfully.');
    }

    public function show(Cart $cart)
    {
        $this->authorize('view', $cart);

        $cart->load('user', 'products');

        $user = auth()->user();
        $cart = $this->cartService->getOrCreateUserCart($user);

        $totalPrice = $cart->totalAmount();

        $defaultAddress = $user->defaultAddress;

        return view('carts.show', compact('cart', 'totalPrice', 'defaultAddress'));
    }


    public function edit(Cart $cart)
    {
        $this->authorize('update', $cart);
        return view('carts.edit', compact('cart'));
    }

    public function update(CartRequest $request, Cart $cart)
    {
        $this->authorize('update', $cart);
        $this->cartService->updateCart($cart->id, $request->validated());
        return redirect()->route('carts.index')->with('success', 'Cart updated successfully.');
    }

    public function destroy(Cart $cart)
    {
        $this->authorize('delete', $cart);
        $this->cartService->deleteCart($cart->id);
        return redirect()->route('carts.index')->with('success', 'Cart deleted successfully.');
    }

    public function addProduct(Cart $cart, Product $product, Request $request)
    {
        $this->authorize('addProduct', $cart);
        $quantity = $request->input('quantity', 1);

        // Логирование для проверки данных
        Log::info('Adding product to cart', [
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'quantity' => $quantity
        ]);

        $this->cartService->addProductToCart($cart, $product, $quantity);

        return redirect()->route('carts.show', $cart->id)->with('success', 'Product added to cart successfully');
    }


    public function removeProduct(Cart $cart, Product $product)
    {
        $this->authorize('removeProduct', $cart);
        $this->cartService->removeProductFromCart($cart, $product);

        return redirect()->route('carts.show', $cart->id)->with('success', 'Product removed from cart successfully');
    }

    public function updateCartItemQuantity(Cart $cart, Product $product, Request $request)
    {
        $this->authorize('update', $cart);
        $quantity = $request->input('quantity');
        $this->cartService->updateCartItemQuantity($cart, $product, $quantity);

        return redirect()->route('carts.show', $cart->id)->with('success', 'Cart item quantity updated successfully');
    }

    public function pay(Cart $cart)
    {
        // Log the attempt to authorize payment
        Log::info('Attempting to authorize payment', ['user_id' => auth()->id()]);

        // Ensure the user's default address is loaded before accessing it
        $address = $cart->user->defaultAddress;

        // Log the user's address for debugging purposes
        Log::info('User Address:', ['address' => $address]);

        // Authorize the payment for the cart
        $this->authorize('pay', $cart);

        // Validate if cart, products, and user are not empty
        if (!$cart || !$cart->products || !$cart->user) {
            return redirect()->route('carts.index')->with('error', 'Корзина не найдена.');
        }

        // Get all products in the cart
        $products = $cart->products;

        // Get the total order amount
        $totalPrice = $cart->totalAmount();

        // Display payment form with cart, products, total amount, and address
        return view('carts.pay', compact('cart', 'products', 'totalPrice', 'address'));
    }
    public function checkout()
    {
        $cart = Cart::where('user_id', Auth::id())->first();
        $products = $cart->products; // Получаем продукты из корзины

        // Передаем данные в OrderController
        return view('checkout', ['products' => $products]);
    }
}
