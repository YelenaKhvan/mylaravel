<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __invoke()
    {
        return view('main');
    }

    /**
     * Show the main page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('main');
    }
}