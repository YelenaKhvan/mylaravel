<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Services\PaymentService;
use App\Http\Requests\PaymentRequest;

class PaymentController extends Controller
{
    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
        $this->authorizeResource(Payment::class, 'payment');
    }

    public function index()
    {
        $this->authorize('viewAny', Payment::class);
        $payments = $this->paymentService->getAllPayments();
        return view('payments.index', compact('payments'));
    }

    public function create()
    {
        $this->authorize('create', Payment::class);
        return view('payments.create');
    }

    public function store(PaymentRequest $request)
    {
        $this->authorize('create', Payment::class);
        $validatedData = $request->validated();
        $this->paymentService->createPayment($validatedData);
        return redirect()->route('payments.index')->with('success', 'Payment created successfully.');
    }

    public function edit(Payment $payment)
    {
        $this->authorize('update', $payment);
        return view('payments.edit', compact('payment'));
    }

    public function update(PaymentRequest $request, Payment $payment)
    {
        $this->authorize('update', $payment);
        $validatedData = $request->validated();
        $this->paymentService->updatePayment($payment->id, $validatedData);
        return redirect()->route('payments.index')->with('success', 'Payment updated successfully.');
    }

    public function destroy(Payment $payment)
    {
        $this->authorize('delete', $payment);
        $this->paymentService->deletePayment($payment->id);
        return redirect()->route('payments.index')->with('success', 'Payment deleted successfully.');
    }
}
