<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Address;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Services\PayboxService;
use Dosarkz\Paybox\Facades\Paybox;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Services\InventoryService;


class PayboxController extends Controller
{
    protected $payboxService;
    protected $orderService;
    protected $inventoryService;

    public function __construct(PayboxService $payboxService, OrderService $orderService, InventoryService $inventoryService)
    {
        $this->payboxService = $payboxService;
        $this->orderService = $orderService;
        $this->inventoryService = $inventoryService; // Инициализируйте свойство
    }

    public function processPayment(Request $request, Cart $cart)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        Log::info('Processing payment for Cart ID: ' . $cart->id . ' by User ID: ' . Auth::id());

        $this->authorize('pay', $cart);

        if ($cart->products->isEmpty()) {
            Log::error('Cart is empty for Cart ID: ' . $cart->id);
            return redirect()->back()->with('error', 'Ваша корзина пуста.');
        }

        $totalPrice = $cart->products->sum(function ($product) {
            return $product->price * $product->pivot->quantity;
        });

        Log::info('Total price calculated: ' . $totalPrice);

        $user = $cart->user;
        $address = Address::where('user_id', $user->id)->where('is_default', true)->first();

        if (!$address) {
            return redirect()->route('addresses.create')->with('error', 'Пожалуйста, добавьте адрес перед оплатой.');
        }

        $orderData = [
            'user_id' => $user->id,
            'address_id' => $address->id,
            'total_amount' => $totalPrice,
            'currency' => 'KZT',
            'status' => 'pending',
            'quantity' => $cart->products->sum('pivot.quantity'),
            'products' => $cart->products->mapWithKeys(function ($product) {
                return [$product->id => ['quantity' => $product->pivot->quantity]];
            })->toArray(),
        ];

        Log::info('Creating order with data:', $orderData);

        try {
            // Создаем заказ через OrderService
            $order = $this->orderService->createOrder($orderData);
            $orderId = $order->order_number; // Используем order_number для Paybox

            // Уменьшаем количество товаров в инвентаре
            $this->inventoryService->decreaseInventory($cart->products->map(function ($product) {
                return ['product_id' => $product->id, 'quantity' => $product->pivot->quantity];
            })->toArray());

            $cart->products()->detach(); // Очистка корзины после создания заказа

            // Сохраняем cart_id в сессии
            session(['cart_id' => $cart->id]);

            $redirectUrl = $this->payboxService->createPayment(
                $totalPrice,
                'KZT',
                'Оплата заказа №' . $orderId,
                $orderId,
                $user->id,
                $cart->id,
                $cart->products->first()->id ?? null,
                $cart->products->sum('pivot.quantity')
            );

            if ($redirectUrl) {
                Log::info('Redirecting to Paybox: ' . $redirectUrl);
                return redirect($redirectUrl);
            } else {
                Log::error('Failed to create payment for Cart ID: ' . $cart->id);
                return redirect()->back()->with('error', 'Ошибка при инициализации платежа.');
            }
        } catch (\Exception $e) {
            Log::error('Order creation failed', ['exception' => $e]);
            return redirect()->back()->with('error', 'Не удалось создать заказ. Попробуйте снова.');
        }
    }

    public function success(Request $request)
    {
        // Логируем все параметры запроса для отладки
        Log::info('All Request Data', ['data' => $request->all()]);

        // Получаем параметры из запроса
        $status = $request->input('pg_status');
        $orderId = $request->input('pg_order_id');
        $paymentId = $request->input('pg_payment_id');

        // Логируем параметры
        Log::info('Processing payment success', [
            'status' => $status,
            'orderId' => $orderId,
            'paymentId' => $paymentId,
        ]);

        // Проверяем, что все необходимые параметры присутствуют
        if (!$orderId) {
            Log::error('Order ID is missing');
            return redirect()->route('home')->with('error', 'Order ID is required.');
        }

        // Получаем cart_id из сессии
        $cartId = session('cart_id');
        session()->forget('cart_id'); // Очищаем сессию

        Log::info('Cart ID from session:', ['cart_id' => $cartId]);

        if (empty($cartId)) {
            Log::error('Cart ID is missing');
            return redirect()->route('home')->with('error', 'Cart ID is required.');
        }

        try {
            $order = Order::where('order_number', $orderId)->first();
            if (!$order) {
                Log::error('Order not found', ['order_number' => $orderId]);
                return redirect()->route('home')->with('error', 'Order not found.');
            }

            // Сразу обновляем статус заказа на 'paid'
            $order->update(['status' => 'paid']);

            // Сохраняем информацию о платеже
            Payment::create([
                'order_id' => $order->id,
                'cart_id' => $cartId,
                'user_id' => $order->user_id,
                'amount' => $request->input('amount') ?? $order->total_amount,
                'payment_method' => 'Paybox',
                'transaction_id' => $paymentId,
            ]);

            Log::info('Payment created', [
                'transaction_id' => $paymentId,
                'order_id' => $order->id,
                'amount' => $request->input('amount') ?? $order->total_amount,
            ]);

            return redirect()->route('home')
                ->with('success', 'Payment processed successfully and order created!');
        } catch (\Exception $e) {
            Log::error('Error processing payment: ' . $e->getMessage(), ['trace' => $e->getTraceAsString()]);
            return redirect()->route('home')->with('error', 'Internal server error.');
        }
    }





    // public function result(Request $request)
    // {
    //     try {
    //         Log::info('Processing payment result', $request->all());

    //         $order = Order::where('order_number', $request->pg_order_id)->first();
    //         if (!$order) {
    //             Log::error('Order not found', ['order_number' => $request->pg_order_id]);
    //             return response()->json(['error' => 'Order not found'], 400);
    //         }

    //         Log::info('Order found', ['order' => $order]);

    //         if ($request->pg_status === 'success') {
    //             $order->update(['status' => 'paid']);
    //             Log::info('Order status updated to paid', ['order' => $order]);

    //             // Ensure cart_id is set
    //             $cartId = $request->cart_id;
    //             if (empty($cartId)) {
    //                 Log::error('Cart ID is missing');
    //                 return response()->json(['error' => 'Cart ID is required'], 400);
    //             }

    //             Payment::create([
    //                 'transaction_id' => $request->pg_payment_id,
    //                 'order_id' => $order->id,
    //                 'cart_id' => $cartId,
    //             ]);
    //             Log::info('Payment created', ['transaction_id' => $request->pg_payment_id, 'order_id' => $order->id]);

    //             return response()->json(['message' => 'Payment processed successfully'], 200);
    //         } else {
    //             $order->update(['status' => 'pending']);
    //             Log::info('Order status updated to pending', ['order' => $order]);

    //             return response()->json(['error' => 'Payment failed'], 400);
    //         }
    //     } catch (\Exception $e) {
    //         Log::error('Error processing payment: ' . $e->getMessage(), ['trace' => $e->getTraceAsString()]);
    //         return response()->json(['error' => 'Internal Server Error'], 500);
    //     }
    // }

    // public function success(Request $request)
    // {
    //     return redirect()->route('home')
    //         ->with('success', 'Платеж успешно обработан и заказ создан!');
    // }

    public function failure(Request $request)
    {
        return redirect()->route('carts.show')->with('error', 'Ошибка при обработке платежа.');
    }
}
