<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShippingRequest;
use App\Models\Shipping;
use App\Services\ShippingServiceInterface;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    protected $service;

    public function __construct(ShippingServiceInterface $service)
    {
        $this->service = $service;
        $this->authorizeResource(Shipping::class, 'shipping');
    }

    public function index()
    {
        $this->authorize('viewAny', Shipping::class);

        // Логика для отображения списка отправок
        $shippings = $this->service->getAll();

        return view('shippings.index', compact('shippings'));
    }

    public function create()
    {
        $this->authorize('create', Shipping::class);

        // Логика для создания новой отправки (необходимо отобразить форму)
        return view('shippings.create');
    }

    public function store(ShippingRequest $request)
    {
        $this->authorize('create', Shipping::class);

        // Логика для сохранения новой отправки
        $this->service->create($request->validated());

        return redirect()->route('shippings.index')
            ->with('success', 'Shipping created successfully.');
    }

    public function show($id)
    {
        $shipping = $this->service->find($id);
        $this->authorize('view', $shipping);

        // Логика для отображения конкретной отправки
        return view('shippings.show', compact('shipping'));
    }

    public function edit($id)
    {
        $shipping = $this->service->find($id);
        $this->authorize('update', $shipping);

        // Логика для редактирования отправки (необходимо отобразить форму)
        return view('shippings.edit', compact('shipping'));
    }

    public function update(ShippingRequest $request, $id)
    {
        $shipping = $this->service->find($id);
        $this->authorize('update', $shipping);

        // Логика для обновления отправки
        $this->service->update($id, $request->validated());

        return redirect()->route('shippings.index')
            ->with('success', 'Shipping updated successfully.');
    }

    public function destroy($id)
    {
        $shipping = $this->service->find($id);
        $this->authorize('delete', $shipping);

        // Логика для удаления отправки
        $this->service->delete($id);

        return redirect()->route('shippings.index')
            ->with('success', 'Shipping deleted successfully.');
    }
}