<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Services\CategoryServiceInterface;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->allWithProducts();
        return view('categories.index', compact('categories'));
    }


    public function create()
    {
        $this->authorize('create', Category::class);
        $categories = $this->categoryService->all();
        return view('categories.create', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $this->authorize('create', Category::class);
        $this->categoryService->create($request->validated());
        return redirect()->route('categories.index')->with('success', 'Категория добавлена!');
    }

    public function show($id)
    {
        $category = $this->categoryService->findById($id);
        return view('categories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = $this->categoryService->findById($id);
        $this->authorize('update', $category);
        $categories = $this->categoryService->all();
        return view('categories.edit', compact('category', 'categories'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = $this->categoryService->findById($id);
        $this->authorize('update', $category);
        $this->categoryService->update($category, $request->validated());
        return redirect()->route('categories.index')->with('success', 'Категория обновлена!');
    }

    public function destroy($id)
    {
        $category = $this->categoryService->findById($id);
        $this->authorize('delete', $category);
        $this->categoryService->delete($category);
        return redirect()->route('categories.index')->with('success', 'Категория удалена!');
    }
}