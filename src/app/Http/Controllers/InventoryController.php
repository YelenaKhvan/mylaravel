<?php

namespace App\Http\Controllers;

use App\Http\Requests\InventoryRequest;
use App\Models\Inventory;
use App\Models\Product;
use App\Services\InventoryService;

class InventoryController extends Controller
{
    protected $inventoryService;

    public function __construct(InventoryService $inventoryService)
    {
        $this->inventoryService = $inventoryService;
        $this->authorizeResource(Inventory::class, 'inventory');
    }

    public function index()
    {
        $inventories = $this->inventoryService->getAllInventories();
        return view('inventories.index', compact('inventories'));
    }

    public function create()
    {
        $products = Product::all(); // Получаем список всех продуктов
        return view('inventories.create', compact('products'));
    }

    public function store(InventoryRequest $request)
    {
        $validatedData = $request->validated();
        $this->inventoryService->createInventory($validatedData);
        return redirect()->route('inventories.index')->with('success', 'Inventory created successfully.');
    }

    public function show(Inventory $inventory)
    {
        return view('inventories.show', compact('inventory'));
    }

    public function edit(Inventory $inventory)
    {
        $products = Product::all(); // Получаем список всех продуктов для редактирования
        return view('inventories.edit', compact('inventory', 'products'));
    }

    public function update(InventoryRequest $request, Inventory $inventory)
    {
        $this->inventoryService->updateInventory($inventory->id, $request->validated());
        return redirect()->route('inventories.index')->with('success', 'Inventory updated successfully.');
    }

    public function destroy(Inventory $inventory)
    {
        $this->inventoryService->deleteInventory($inventory->id);
        return redirect()->route('inventories.index')->with('success', 'Inventory deleted successfully.');
    }
}
