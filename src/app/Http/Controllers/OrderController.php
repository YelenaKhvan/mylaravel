<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\User;
use App\Models\Order;
use App\Models\Address;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Services\InventoryService;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    protected $orderService;
    protected $inventoryService;

    public function __construct(OrderService $orderService, InventoryService $inventoryService)
    {
        $this->orderService = $orderService;
        $this->inventoryService = $inventoryService;
        // $this->authorizeResource(Order::class, 'order');
    }

    public function index()
    {
        $orders = $this->orderService->getAllOrders();
        return view('orders.index', compact('orders'));
    }

    public function create()
    {
        $users = User::all();
        $products = Product::all();
        return view('orders.create', compact('users', 'products'));
    }

    public function store(OrderRequest $request)
    {
        $orderData = $request->all();

        Log::info('Creating order with data:', $orderData);

        try {
            $order = $this->orderService->createOrder($orderData);
            return redirect()->route('orders.show', $order->id)->with('success', 'Заказ создан успешно.');
        } catch (\Exception $e) {
            Log::error('Order creation failed', ['exception' => $e]);
            return redirect()->back()->with('error', 'Не удалось создать заказ. Попробуйте снова.');
        }
    }

    public function show(Order $order)
    {
        $order = Order::with('products')->findOrFail($order->id);
        Log::info('Order Details:', ['order' => $order]);
        return view('orders.show', compact('order'));
    }

    public function edit(Order $order)
    {
        $users = User::all();
        $products = Product::all();
        return view('orders.edit', compact('order', 'users', 'products'));
    }

    public function update(OrderRequest $request, Order $order)
    {
        $validatedData = $request->validated();

        if (!$this->inventoryService->checkInventory($validatedData['product_id'], $validatedData['quantity'])) {
            return redirect()->back()->withErrors('Insufficient inventory for the selected product.');
        }

        $this->orderService->updateOrder($order->id, $validatedData);
        return redirect()->route('orders.index')->with('success', 'Order updated successfully.');
    }

    public function destroy(Order $order)
    {
        $this->orderService->deleteOrder($order->id);
        return redirect()->route('orders.index')->with('success', 'Заказ отменен успешно.');
    }

    private function generateOrderNumber()
    {
        return 'ORD-' . strtoupper(uniqid());
    }

    public function cancel(Order $order)
    {
        if ($order->status === 'completed' || $order->status === 'shipped') {
            return redirect()->back()->withErrors('Этот заказ не может быть отменен.');
        }

        $order->update(['status' => 'canceled']);
        $this->inventoryService->increaseInventory($order->id); // Восстановление запасов, если это необходимо

        return redirect()->route('orders.index')->with('success', 'Заказ отменен успешно.');
    }
}
