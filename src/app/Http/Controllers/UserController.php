<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\UserServiceInterface;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }


    // public function index()
    // {
    //     $this->authorize('viewAny', User::class);

    //     // Получаем всех пользователей без сортировки
    //     $users = $this->userService->getAllUsers(); // Метод getAllUsers без параметров

    //     // Логируем результат для проверки
    //     Log::info('Users retrieved: ', $users->toArray());

    //     return view('users.index', compact('users'));
    // }
    public function index(Request $request)
    {
        $sortBy = $request->get('sort_by', 'name');
        $sortOrder = $request->get('sort_order', 'asc');

        $users = User::orderBy($sortBy, $sortOrder)->paginate(15);

        Log::info('Users retrieved: ', $users->toArray());

        return view('users.index', compact('users'));
    }



    public function create()
    {
        $this->authorize('create', User::class);

        return view('users.create');
    }

    public function store(UserRequest $request)
    {
        $this->authorize('create', User::class);

        $validatedData = $request->validated();
        $this->userService->createUser($validatedData);
        return redirect()->route('users.index');
    }

    public function show(string $id)
    {
        $user = $this->userService->getUserById($id);
        $this->authorize('view', $user);

        return view('users.show', compact('user'));
    }

    public function edit(string $id)
    {
        $user = $this->userService->getUserById($id);
        $this->authorize('update', $user);

        return view('users.edit', compact('user'));
    }

    public function update(UserRequest $request, $id)
    {
        $validatedData = $request->validated();

        // Попытка получить пользователя по id
        $user = $this->userService->getUserById($id);

        // Проверка, был ли пользователь найден
        if (!$user) {
            return redirect()->route('users.index')->with('error', 'User not found.');
        }

        // Проверка авторизации
        $this->authorize('update', $user);

        // Убираем пустые значения и неизмененные поля из массива $validatedData
        foreach ($validatedData as $key => $value) {
            if ($value === '' || $value === null || $value === $user->$key) {
                unset($validatedData[$key]);
            }
        }

        // Обновление пользователя
        $this->userService->updateUser($user, $validatedData);

        return redirect()->route('users.index')->with('success', 'User updated successfully.');
    }



    public function destroy($id)
    {
        $user = $this->userService->getUserById($id);
        $this->authorize('delete', $user);

        $this->userService->deleteUser($id);

        return redirect()->route('users.index');
    }

    public function export(Request $request)
    {
        $this->authorize('viewAny', User::class);

        $sortBy = $request->input('sort_by', 'name');
        $sortOrder = $request->input('sort_order', 'asc');

        // Используем метод getAllUsersSorted для передачи отсортированных пользователей в экспорт
        $users = $this->userService->getAllUsersSorted($sortBy, $sortOrder);

        return Excel::download(new UsersExport($users), 'users.xlsx');
    }
}
