<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;


class LogoutController extends BaseController
{
    /**
     * Logout a user (Revoke the token).
     *
     * Logs out the currently authenticated user by revoking all tokens.
     *
     * @authenticated
     * 
     * @response 200 {
     *     "message": "Logged out successfully"
     * }
     * @response 401 {
     *     "error": "Unauthenticated"
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();

        return response()->json([
            'message' => 'Logged out successfully'
        ]);
    }
}
