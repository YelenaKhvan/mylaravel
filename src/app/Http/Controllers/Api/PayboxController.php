<?php

namespace App\Http\Controllers\Api;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Address;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Services\PayboxService;
use App\Services\InventoryService;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PayboxController extends BaseController
{
    protected $payboxService;
    protected $orderService;
    protected $inventoryService;

    public function __construct(PayboxService $payboxService, OrderService $orderService, InventoryService $inventoryService)
    {
        $this->payboxService = $payboxService;
        $this->orderService = $orderService;
        $this->inventoryService = $inventoryService;
    }

    /**
     * @OA\Post(
     *     path="/api/paybox/process-payment",
     *     summary="Process Payment",
     *     description="Handles payment processing for the cart.",
     *     tags={"Payment"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"cart_id"},
     *             @OA\Property(property="cart_id", type="integer", example=1),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Payment processing initiated",
     *         @OA\JsonContent(
     *             @OA\Property(property="redirect_url", type="string", example="https://paybox.example.com/redirect")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Invalid cart ID or cart is empty.")
     *         )
     *     )
     * )
     */
    public function processPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cart_id' => 'required|integer|exists:carts,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $cart = Cart::findOrFail($request->cart_id);

        if (!Auth::check()) {
            return response()->json(['error' => 'User not authenticated.'], 401);
        }

        Log::info('Processing payment for Cart ID: ' . $cart->id . ' by User ID: ' . Auth::id());

        if ($cart->products->isEmpty()) {
            Log::error('Cart is empty for Cart ID: ' . $cart->id);
            return response()->json(['error' => 'Cart is empty.'], 400);
        }

        $totalPrice = $cart->products->sum(function ($product) {
            return $product->price * $product->pivot->quantity;
        });

        $user = $cart->user;
        $address = Address::where('user_id', $user->id)->where('is_default', true)->first();

        if (!$address) {
            return response()->json(['error' => 'Please add a default address before payment.'], 400);
        }

        $orderData = [
            'user_id' => $user->id,
            'address_id' => $address->id,
            'total_amount' => $totalPrice,
            'currency' => 'KZT',
            'status' => 'pending',
            'quantity' => $cart->products->sum('pivot.quantity'),
            'products' => $cart->products->mapWithKeys(function ($product) {
                return [$product->id => ['quantity' => $product->pivot->quantity]];
            })->toArray(),
        ];

        try {
            $order = $this->orderService->createOrder($orderData);
            $orderId = $order->order_number;

            $this->inventoryService->decreaseInventory($cart->products->map(function ($product) {
                return ['product_id' => $product->id, 'quantity' => $product->pivot->quantity];
            })->toArray());

            $cart->products()->detach();

            $redirectUrl = $this->payboxService->createPayment(
                $totalPrice,
                'KZT',
                'Payment for Order №' . $orderId,
                $orderId,
                $user->id,
                $cart->id,
                $cart->products->first()->id ?? null,
                $cart->products->sum('pivot.quantity')
            );

            if ($redirectUrl) {
                return response()->json(['redirect_url' => $redirectUrl], 200);
            } else {
                Log::error('Failed to create payment for Cart ID: ' . $cart->id);
                return response()->json(['error' => 'Error initiating payment.'], 500);
            }
        } catch (\Exception $e) {
            Log::error('Order creation failed', ['exception' => $e]);
            return response()->json(['error' => 'Failed to create order. Please try again.'], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/paybox/success",
     *     summary="Payment Success",
     *     description="Handles payment success callback from Paybox.",
     *     tags={"Payment"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="pg_order_id", type="string", example="ORD123456"),
     *             @OA\Property(property="pg_payment_id", type="string", example="PAY123456"),
     *             @OA\Property(property="amount", type="number", format="float", example=99.99),
     *             @OA\Property(property="pg_status", type="string", example="success"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Payment processed successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Payment processed successfully.")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error processing payment",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Error processing payment.")
     *         )
     *     )
     * )
     */
    public function success(Request $request)
    {
        Log::info('Payment success request data:', $request->all());

        $status = $request->input('pg_status');
        $orderId = $request->input('pg_order_id');
        $paymentId = $request->input('pg_payment_id');

        if (!$orderId) {
            Log::error('Order ID is missing');
            return response()->json(['error' => 'Order ID is required.'], 400);
        }

        $cartId = session('cart_id');
        session()->forget('cart_id');

        if (empty($cartId)) {
            Log::error('Cart ID is missing');
            return response()->json(['error' => 'Cart ID is required.'], 400);
        }

        try {
            $order = Order::where('order_number', $orderId)->first();
            if (!$order) {
                Log::error('Order not found', ['order_number' => $orderId]);
                return response()->json(['error' => 'Order not found.'], 404);
            }

            $order->update(['status' => 'paid']);

            Payment::create([
                'order_id' => $order->id,
                'cart_id' => $cartId,
                'user_id' => $order->user_id,
                'amount' => $request->input('amount') ?? $order->total_amount,
                'payment_method' => 'Paybox',
                'transaction_id' => $paymentId,
            ]);

            Log::info('Payment created', [
                'transaction_id' => $paymentId,
                'order_id' => $order->id,
                'amount' => $request->input('amount') ?? $order->total_amount,
            ]);

            return response()->json(['message' => 'Payment processed successfully and order created!'], 200);
        } catch (\Exception $e) {
            Log::error('Error processing payment: ' . $e->getMessage(), ['trace' => $e->getTraceAsString()]);
            return response()->json(['error' => 'Internal server error.'], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/paybox/failure",
     *     summary="Payment Failure",
     *     description="Handles payment failure callback from Paybox.",
     *     tags={"Payment"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="pg_order_id", type="string", example="ORD123456"),
     *             @OA\Property(property="pg_payment_id", type="string", example="PAY123456"),
     *             @OA\Property(property="amount", type="number", format="float", example=99.99),
     *             @OA\Property(property="pg_status", type="string", example="failure"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Payment failure processed",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Payment failed. Please try again.")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error processing failure",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Error processing payment failure.")
     *         )
     *     )
     * )
     */
    public function failure(Request $request)
    {
        Log::info('Payment failure request data:', $request->all());

        $status = $request->input('pg_status');
        $orderId = $request->input('pg_order_id');
        $paymentId = $request->input('pg_payment_id');

        if (!$orderId) {
            Log::error('Order ID is missing');
            return response()->json(['error' => 'Order ID is required.'], 400);
        }

        if (empty($request->session()->get('cart_id'))) {
            Log::error('Cart ID is missing');
            return response()->json(['error' => 'Cart ID is required.'], 400);
        }

        try {
            $order = Order::where('order_number', $orderId)->first();
            if ($order) {
                $order->update(['status' => 'failed']);
            }

            Log::info('Payment failure processed', [
                'transaction_id' => $paymentId,
                'order_id' => $orderId,
            ]);

            return response()->json(['message' => 'Payment failed. Please try again.'], 200);
        } catch (\Exception $e) {
            Log::error('Error processing payment failure: ' . $e->getMessage(), ['trace' => $e->getTraceAsString()]);
            return response()->json(['error' => 'Internal server error.'], 500);
        }
    }

    // public function result(Request $request)
    // {
    //     try {
    //         Log::info('Processing payment result', $request->all());

    //         $order = Order::where('order_number', $request->pg_order_id)->first();
    //         if (!$order) {
    //             Log::error('Order not found', ['order_number' => $request->pg_order_id]);
    //             return response()->json(['error' => 'Order not found'], 400);
    //         }

    //         Log::info('Order found', ['order' => $order]);

    //         if ($request->pg_status === 'success') {
    //             $order->update(['status' => 'paid']);
    //             Log::info('Order status updated to paid', ['order' => $order]);

    //             // Ensure cart_id is set
    //             $cartId = $request->cart_id;
    //             if (empty($cartId)) {
    //                 Log::error('Cart ID is missing');
    //                 return response()->json(['error' => 'Cart ID is required'], 400);
    //             }

    //             Payment::create([
    //                 'transaction_id' => $request->pg_payment_id,
    //                 'order_id' => $order->id,
    //                 'cart_id' => $cartId,
    //             ]);
    //             Log::info('Payment created', ['transaction_id' => $request->pg_payment_id, 'order_id' => $order->id]);

    //             return response()->json(['message' => 'Payment processed successfully'], 200);
    //         } else {
    //             $order->update(['status' => 'pending']);
    //             Log::info('Order status updated to pending', ['order' => $order]);

    //             return response()->json(['error' => 'Payment failed'], 400);
    //         }
    //     } catch (\Exception $e) {
    //         Log::error('Error processing payment: ' . $e->getMessage(), ['trace' => $e->getTraceAsString()]);
    //         return response()->json(['error' => 'Internal Server Error'], 500);
    //     }
    // }

    // public function success(Request $request)
    // {
    //     return redirect()->route('home')
    //         ->with('success', 'Платеж успешно обработан и заказ создан!');
    // }


}
