<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\CartServiceInterface;
use Illuminate\Support\Facades\Log;



/**
 * @OA\Tag(
 *     name="Carts",
 *     description="Operations related to carts"
 * )
 */
class CartController extends BaseController
{
    protected $cartService;

    public function __construct(CartServiceInterface $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @OA\Get(
     *     path="/api/carts/{id}",
     *     tags={"Carts"},
     *     summary="Get a cart by ID",
     *     description="Returns a single cart",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Cart ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Cart details",
     *         @OA\JsonContent(ref="#/components/schemas/Cart")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Cart not found"
     *     )
     * )
     */
    public function show(Cart $cart)
    {
        $this->authorize('view', $cart);

        $cart->load('user', 'products');

        $user = auth()->user();
        $cart = $this->cartService->getOrCreateUserCart($user);

        $totalPrice = $cart->totalAmount();

        $defaultAddress = $user->defaultAddress;

        return response()->json(compact('cart', 'totalPrice', 'defaultAddress'));
    }

    /**
     * @OA\Post(
     *     path="/api/carts/{cart}/products/{product}",
     *     tags={"Carts"},
     *     summary="Add a product to the cart",
     *     description="Adds a product to the cart",
     *     @OA\Parameter(
     *         name="cart",
     *         in="path",
     *         required=true,
     *         description="Cart ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="product",
     *         in="path",
     *         required=true,
     *         description="Product ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="quantity", type="integer", example=1)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Product added to cart",
     *         @OA\JsonContent(ref="#/components/schemas/Cart")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Cart or Product not found"
     *     )
     * )
     */
    public function addProduct(Cart $cart, Product $product, Request $request)
    {
        $this->authorize('addProduct', $cart);
        $quantity = $request->input('quantity', 1);

        Log::info('Adding product to cart', [
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'quantity' => $quantity
        ]);

        $this->cartService->addProductToCart($cart, $product, $quantity);

        return response()->json(['success' => 'Product added to cart successfully']);
    }

    /**
     * @OA\Delete(
     *     path="/api/carts/{cart}/products/{product}",
     *     tags={"Carts"},
     *     summary="Remove a product from the cart",
     *     description="Removes a product from the cart",
     *     @OA\Parameter(
     *         name="cart",
     *         in="path",
     *         required=true,
     *         description="Cart ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="product",
     *         in="path",
     *         required=true,
     *         description="Product ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Product removed from cart",
     *         @OA\JsonContent(ref="#/components/schemas/Cart")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Cart or Product not found"
     *     )
     * )
     */
    public function removeProduct(Cart $cart, Product $product)
    {
        $this->authorize('removeProduct', $cart);
        $this->cartService->removeProductFromCart($cart, $product);

        return response()->json(['success' => 'Product removed from cart successfully']);
    }

    /**
     * @OA\Patch(
     *     path="/api/carts/{cart}/products/{product}",
     *     tags={"Carts"},
     *     summary="Update quantity of a product in the cart",
     *     description="Updates the quantity of a product in the cart",
     *     @OA\Parameter(
     *         name="cart",
     *         in="path",
     *         required=true,
     *         description="Cart ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="product",
     *         in="path",
     *         required=true,
     *         description="Product ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="quantity", type="integer", example=2)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Product quantity updated",
     *         @OA\JsonContent(ref="#/components/schemas/Cart")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Cart or Product not found"
     *     )
     * )
     */
    public function updateCartItemQuantity(Cart $cart, Product $product, Request $request)
    {
        $this->authorize('update', $cart);
        $quantity = $request->input('quantity');
        $this->cartService->updateCartItemQuantity($cart, $product, $quantity);

        return response()->json(['success' => 'Cart item quantity updated successfully']);
    }

    /**
     * @OA\Post(
     *     path="/api/carts/{cart}/pay",
     *     tags={"Carts"},
     *     summary="Pay for the cart",
     *     description="Handles the payment process for the cart",
     *     @OA\Parameter(
     *         name="cart",
     *         in="path",
     *         required=true,
     *         description="Cart ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Payment processed",
     *         @OA\JsonContent(ref="#/components/schemas/Cart")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Cart not found"
     *     )
     * )
     */
    public function pay(Cart $cart)
    {
        Log::info('Attempting to authorize payment', ['user_id' => auth()->id()]);

        $address = $cart->user->defaultAddress;

        Log::info('User Address:', ['address' => $address]);

        $this->authorize('pay', $cart);

        if (!$cart || !$cart->products || !$cart->user) {
            return response()->json(['error' => 'Корзина не найдена.'], 404);
        }

        $products = $cart->products;
        $totalPrice = $cart->totalAmount();

        return response()->json(compact('cart', 'products', 'totalPrice', 'address'));
    }
}
