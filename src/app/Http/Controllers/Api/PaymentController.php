<?php

namespace App\Http\Controllers\Api;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Services\PaymentService;
use App\Http\Requests\PaymentRequest;
use App\Http\Controllers\Controller;

/**
 * @OA\Tag(
 *     name="Payments",
 *     description="API endpoints for managing payments"
 * )
 */
class PaymentController extends Controller
{
    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
        $this->authorizeResource(Payment::class, 'payment');
    }

    /**
     * @OA\Get(
     *     path="/api/payments",
     *     summary="Get all payments",
     *     tags={"Payments"},
     *     @OA\Response(
     *         response=200,
     *         description="A list of payments",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Payment")
     *         )
     *     )
     * )
     */
    public function index()
    {
        $this->authorize('viewAny', Payment::class);
        $payments = $this->paymentService->getAllPayments();
        return response()->json($payments);
    }

    /**
     * @OA\Get(
     *     path="/api/payments/{payment}",
     *     summary="Get payment details",
     *     description="Returns details of a specific payment",
     *     tags={"Payments"},
     *     @OA\Parameter(
     *         name="payment",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/Payment")
     *     ),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not Found")
     * )
     */
    public function show(Payment $payment)
    {
        $this->authorize('view', $payment);
        return response()->json($payment);
    }

    /**
     * @OA\Post(
     *     path="/api/payments",
     *     summary="Store payment",
     *     description="Stores a new payment",
     *     tags={"Payments"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Payment")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Payment created successfully",
     *         @OA\JsonContent(ref="#/components/schemas/Payment")
     *     ),
     *     @OA\Response(response=400, description="Invalid input"),
     *     @OA\Response(response=403, description="Forbidden")
     * )
     */
    public function store(PaymentRequest $request)
    {
        $this->authorize('create', Payment::class);
        $validatedData = $request->validated();
        $payment = $this->paymentService->createPayment($validatedData);
        return response()->json($payment, 201);
    }

    /**
     * @OA\Put(
     *     path="/api/payments/{payment}",
     *     summary="Update payment",
     *     description="Updates an existing payment",
     *     tags={"Payments"},
     *     @OA\Parameter(
     *         name="payment",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Payment")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Payment updated successfully",
     *         @OA\JsonContent(ref="#/components/schemas/Payment")
     *     ),
     *     @OA\Response(response=400, description="Invalid input"),
     *     @OA\Response(response=403, description="Forbidden")
     * )
     */
    public function update(PaymentRequest $request, Payment $payment)
    {
        $this->authorize('update', $payment);
        $validatedData = $request->validated();
        $updatedPayment = $this->paymentService->updatePayment($payment->id, $validatedData);
        return response()->json($updatedPayment);
    }

    /**
     * @OA\Delete(
     *     path="/api/payments/{payment}",
     *     summary="Delete payment",
     *     description="Deletes a payment",
     *     tags={"Payments"},
     *     @OA\Parameter(
     *         name="payment",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Payment deleted successfully"
     *     ),
     *     @OA\Response(response=403, description="Forbidden")
     * )
     */
    public function destroy(Payment $payment)
    {
        $this->authorize('delete', $payment);
        $this->paymentService->deletePayment($payment->id);
        return response()->json(null, 204);
    }
}
