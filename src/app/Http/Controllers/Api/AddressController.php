<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Models\Address;
use App\Services\AddressService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddressRequest;
use Illuminate\Http\Request;


/**
 * @OA\Tag(
 *     name="Addresses",
 *     description="Operations related to addresses"
 * )
 */
class AddressController extends BaseController
{
    protected $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    /**
     * @OA\Get(
     *     path="/api/addresses",
     *     tags={"Addresses"},
     *     summary="Get all addresses",
     *     description="Returns a list of addresses",
     *     @OA\Response(
     *         response=200,
     *         description="List of addresses",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Address"))
     *     )
     * )
     */
    public function index()
    {
        $this->authorize('viewAny', Address::class);

        $addresses = $this->addressService->getAllAddresses();
        return response()->json($addresses);
    }

    /**
     * @OA\Get(
     *     path="/api/addresses/{id}",
     *     tags={"Addresses"},
     *     summary="Get address by ID",
     *     description="Returns a single address",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Address ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Address details",
     *         @OA\JsonContent(ref="#/components/schemas/Address")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     )
     * )
     */
    public function show($id)
    {
        $address = $this->addressService->getAddressById($id);
        $this->authorize('view', $address);

        return response()->json($address);
    }

    /**
     * @OA\Post(
     *     path="/api/addresses",
     *     tags={"Addresses"},
     *     summary="Create a new address",
     *     description="Stores a new address",
     *     @OA\RequestBody(
     *         required=true,
     *         
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Address created successfully",
     *         @OA\JsonContent(ref="#/components/schemas/Address")
     *     )
     * )
     */
    public function store(AddressRequest $request)
    {
        $this->authorize('create', Address::class);

        $data = $request->validated();
        $data['user_id'] = auth()->id();
        $data['is_default'] = isset($data['is_default']) && $data['is_default'] ? 1 : 0;

        if ($data['is_default']) {
            Address::where('user_id', auth()->id())->update(['is_default' => 0]);
        }

        $address = $this->addressService->createAddress($data);

        return response()->json($address, 201);
    }

    /**
     * @OA\Put(
     *     path="/api/addresses/{id}",
     *     tags={"Addresses"},
     *     summary="Update an address",
     *     description="Updates an existing address",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Address ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *        
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Address updated successfully",
     *         @OA\JsonContent(ref="#/components/schemas/Address")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     )
     * )
     */
    public function update(AddressRequest $request, $id)
    {
        $validatedData = $request->validated();

        $address = $this->addressService->getAddressById($id);

        if (!$address) {
            return response()->json(['error' => 'Address not found.'], 404);
        }

        $this->authorize('update', $address);

        foreach ($validatedData as $key => $value) {
            if ($value === '' || $value === null || $value === $address->$key) {
                unset($validatedData[$key]);
            }
        }

        $address = $this->addressService->updateAddress($address, $validatedData);

        return response()->json($address);
    }

    /**
     * @OA\Delete(
     *     path="/api/addresses/{id}",
     *     tags={"Addresses"},
     *     summary="Delete an address",
     *     description="Deletes an address by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Address ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Address deleted successfully"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     )
     * )
     */
    public function destroy($id)
    {
        $address = $this->addressService->getAddressById($id);
        $this->authorize('delete', $address);

        $this->addressService->deleteAddress($address);

        return response()->json(null, 204);
    }

    /**
     * @OA\Post(
     *     path="/api/addresses/{id}/restore",
     *     tags={"Addresses"},
     *     summary="Restore a deleted address",
     *     description="Restores a previously deleted address",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Address ID",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Address restored successfully",
     *         @OA\JsonContent(ref="#/components/schemas/Address")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Address not found"
     *     )
     * )
     */
    public function restore($id)
    {
        $address = Address::onlyTrashed()->findOrFail($id);
        $this->authorize('restore', $address);

        $address = $this->addressService->restoreAddress($id);

        return response()->json($address);
    }
}
