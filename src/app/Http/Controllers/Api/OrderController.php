<?php

namespace App\Http\Controllers\Api;


use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Services\InventoryService;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\BaseController;

class OrderController extends BaseController
{
    protected $orderService;
    protected $inventoryService;

    public function __construct(OrderService $orderService, InventoryService $inventoryService)
    {
        $this->orderService = $orderService;
        $this->inventoryService = $inventoryService;
        // $this->authorizeResource(Order::class, 'order');
    }

    /**
     * @OA\Get(
     *     path="/api/orders",
     *     summary="Get All Orders",
     *     description="Retrieve a list of all orders.",
     *     tags={"Order"},
     *     @OA\Response(
     *         response=200,
     *         description="List of orders",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Order")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Unauthorized")
     *         )
     *     )
     * )
     */
    public function index()
    {
        $orders = $this->orderService->getAllOrders();
        return response()->json($orders);
    }

    /**
     * @OA\Post(
     *     path="/api/orders",
     *     summary="Create an Order",
     *     description="Create a new order with the provided data.",
     *     tags={"Order"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"user_id", "product_id", "quantity"},
     *             @OA\Property(property="user_id", type="integer", example=1),
     *             @OA\Property(property="product_id", type="integer", example=1),
     *             @OA\Property(property="quantity", type="integer", example=2),
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Order created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="order_id", type="integer", example=1)
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Invalid input data")
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|integer|exists:users,id',
            'product_id' => 'required|integer|exists:products,id',
            'quantity' => 'required|integer|min:1',
        ]);

        Log::info('Creating order with data:', $validatedData);

        try {
            $order = $this->orderService->createOrder($validatedData);
            return response()->json(['order_id' => $order->id], 201);
        } catch (\Exception $e) {
            Log::error('Order creation failed', ['exception' => $e]);
            return response()->json(['error' => 'Unable to create order. Please try again.'], 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/orders/{id}",
     *     summary="Get Order Details",
     *     description="Retrieve details of a specific order.",
     *     tags={"Order"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order details",
     *         @OA\JsonContent(ref="#/components/schemas/Order")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Order not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Order not found.")
     *         )
     *     )
     * )
     */
    public function show(Order $order)
    {
        $order = Order::with('products')->findOrFail($order->id);
        Log::info('Order Details:', ['order' => $order]);
        return response()->json($order);
    }

    /**
     * @OA\Put(
     *     path="/api/orders/{id}",
     *     summary="Update an Order",
     *     description="Update the details of a specific order.",
     *     tags={"Order"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"product_id", "quantity"},
     *             @OA\Property(property="product_id", type="integer", example=1),
     *             @OA\Property(property="quantity", type="integer", example=2),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order updated successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Order updated successfully.")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input or insufficient inventory",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Invalid input or insufficient inventory.")
     *         )
     *     )
     * )
     */
    public function update(Request $request, Order $order)
    {
        $validatedData = $request->validate([
            'product_id' => 'required|integer|exists:products,id',
            'quantity' => 'required|integer|min:1',
        ]);

        if (!$this->inventoryService->checkInventory($validatedData['product_id'], $validatedData['quantity'])) {
            return response()->json(['error' => 'Insufficient inventory for the selected product.'], 400);
        }

        $this->orderService->updateOrder($order->id, $validatedData);
        return response()->json(['message' => 'Order updated successfully.']);
    }

    /**
     * @OA\Delete(
     *     path="/api/orders/{id}",
     *     summary="Delete an Order",
     *     description="Delete a specific order.",
     *     tags={"Order"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Order canceled successfully.")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Order not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Order not found.")
     *         )
     *     )
     * )
     */
    public function destroy(Order $order)
    {
        $this->orderService->deleteOrder($order->id);
        return response()->json(['message' => 'Order canceled successfully.']);
    }

    /**
     * @OA\Post(
     *     path="/api/orders/{id}/cancel",
     *     summary="Cancel an Order",
     *     description="Cancel a specific order.",
     *     tags={"Order"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order canceled successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Order canceled successfully.")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Order cannot be canceled",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Order cannot be canceled.")
     *         )
     *     )
     * )
     */
    public function cancel(Order $order)
    {
        if ($order->status === 'completed' || $order->status === 'shipped') {
            return response()->json(['error' => 'This order cannot be canceled.'], 400);
        }

        $order->update(['status' => 'canceled']);
        $this->inventoryService->increaseInventory($order->id);

        return response()->json(['message' => 'Order canceled successfully.']);
    }
}