<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;


/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="Shop API Documentation",
 *     description="API documentation for the Shop-project",
 *     @OA\Contact(
 *         email="helen.khvan89@gmail.com"
 *     )
 * )
 */
class BaseController extends Controller
{
}
