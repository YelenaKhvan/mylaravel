<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'address_id' => 'required|integer',
            'total_amount' => 'required|numeric',
            'currency' => 'required|string',
            'status' => 'required|string',
            'quantity' => 'required|integer',
            'products' => 'required|array',
            'products.*.quantity' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'total_amount.required' => 'The total amount is required.',
            'total_amount.numeric' => 'The total amount must be a number.',
            // other messages
        ];
    }
}
