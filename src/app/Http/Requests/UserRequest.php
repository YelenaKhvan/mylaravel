<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $userId = $this->route('id');

        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $userId,
            'age' => 'required|integer|min:0',
            'phone' => 'nullable|string|max:20', // Изменение на nullable
            'is_admin' => 'required|boolean',
        ];

        if ($this->isMethod('post')) {
            $rules['password'] = 'required|string|min:8|confirmed|regex:/^[a-zA-Z0-9]+$/';
        } elseif ($this->isMethod('patch') || $this->isMethod('put')) {
            $rules['password'] = 'nullable|string|min:8|confirmed|regex:/^[a-zA-Z0-9]+$/'; // Изменение на nullable
        }

        return $rules;
    }
}