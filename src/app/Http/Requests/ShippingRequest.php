<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShippingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'address_id' => 'required|exists:addresses,id',
            'user_id' => 'required|exists:users,id',
            'shipping_fee' => 'required|numeric|min:0',
            'order_total' => 'required|numeric|min:0',
            'is_free' => 'boolean',
        ];
    }
}
