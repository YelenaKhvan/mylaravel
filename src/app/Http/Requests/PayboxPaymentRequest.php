<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayboxPaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:1',
            'currency' => 'required|string|max:3',
            'description' => 'required|string|max:255',
            'user_id' => 'required|exists:users,id',
        ];
    }
}